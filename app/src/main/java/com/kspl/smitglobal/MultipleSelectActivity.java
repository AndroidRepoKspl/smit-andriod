package com.kspl.smitglobal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.kspl.smitglobal.ApplicationClass.BaseActivity;
import com.kspl.smitglobal.databinding.ActivityMultipleSelectBinding;
import com.kspl.smitglobal.modelClasses.products.Product;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

//initial commit
public class MultipleSelectActivity extends AppCompatActivity implements
        ProductClickListener,
        ProductAdapter.deleteProduct,
        ProductAdapter.editProduct,
        ProductAdapter.ProductDetails
{

    private List<Product> productList = new ArrayList<>();
    private Context context;

    private ActivityMultipleSelectBinding binding;
    public static Boolean isSelectionModeOn = false;
    private ProductAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_multiple_select);
        context = this;

        //   productList = getList();

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ProductAdapter(productList, this, this, this, this);
        binding.recyclerView.setAdapter(adapter);

        binding.btnSelectionMode.setOnClickListener(view -> {
            if (isSelectionModeOn) {
                isSelectionModeOn = false;
                binding.btnSelectionMode.setText("Turn on Selection Mode");
                binding.btnSend.setVisibility(View.GONE);
                //       adapter.updateList(getList());

            } else {
                isSelectionModeOn = true;
                binding.btnSelectionMode.setText("Turn off Selection Mode");
                binding.btnSend.setVisibility(View.VISIBLE);


            }
        });




      /*  binding.btnSend.setOnClickListener(view ->{
            List<Product> selectedProductList = adapter.getSelectedProducts();
            StringBuilder builder = new StringBuilder();
            for(int i =0; i<selectedProductList.size(); i++){
                if(i==0){
                     builder.append(selectedProductList.get(i).name);
                }else
                {
                    builder.append("\n").append(selectedProductList.get(i).name);
                }
            }
            Toast.makeText(context, ""+builder.toString(), Toast.LENGTH_SHORT).show();
        });*/

    }

/*    private List<Product> getList() {
        List<Product> list = new ArrayList<>();
        for(int i = 0; i<10; i++){
            list.add(new Product("product "+i,"type "+i,false));
        }
        return list;
    }*/

    @Override
    public void onProductClickAction(Boolean isSelected) {
        if (isSelected) {
            binding.btnSend.setVisibility(View.VISIBLE);
            isSelectionModeOn = true;
            binding.btnSelectionMode.setText("Turn off Selection Mode");
        } else {
            binding.btnSend.setVisibility(View.GONE);
        }
    }

    @Override
    public void onImageClicked(Product product) {

    }

    @Override
    public void onEnquiryClick(Product product) {

    }

    @Override
    public void onDeleteClick(Product product) {

    }


    @Override
    public void deleteProductClick(@NotNull Product product) {

    }

    @Override
    public void editProductClick(@NotNull Product product) {

    }

    @Override
    public void productDetailsClick(@NotNull Product product) {
    }
}