package com.kspl.smitglobal;

import com.kspl.smitglobal.modelClasses.products.Product;

public interface ProductClickListener {
    void onProductClickAction(Boolean isSelected);
    void onImageClicked(Product product);
    void onEnquiryClick(Product product);
    void onDeleteClick(Product product);

}
