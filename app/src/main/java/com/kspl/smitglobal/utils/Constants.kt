package com.kspl.smitglobal.utils

import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import com.kspl.smitglobal.ApplicationClass.App.Companion.appContext
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.GradeResponse.ProductGrade
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategory
import com.preference.PowerPreference
import java.util.HashMap

class Constants {

    companion object {

        const val DATEFORMAT = "yyyy-MM-dd"

        const val SUCCESS = "success"
        const val NULL_RESPONSE = "No Response from server"
        const val NO_INTERNET = "No Internet Connection"
        const val LOGIN = "Login"
        const val LOGIN_CONTENT = "Please wait, while checking Username and Password!"
        const val HOME = "Give And Ask"
        const val HOME_CONTENT = "Please wait, while loading data"
        const val SEARCH_TIME_DELAY = 500L
        var isSelectionModeOn = false


        lateinit var dialog: ProgressDialog


        fun showProgressDialog(context: Context, title: String, msg: String) {
            dialog = ProgressDialog.show(
                context,
                title,
                msg,
                false,
                false
            )
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
        }

        fun getHeadersWithoutToken(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            headers["Accept"] = "application/json"
            return headers
        }


        fun getHeadersWithToken(): HashMap<String, String> {

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            headers["Accept"] = "application/json"
            headers["Authorization"] = "Bearer ${
            PowerPreference.getDefaultFile()
                .getString("token")}"
            return headers
        }


        fun isNetworkAvailable(): Boolean {
            val connectivityManager =
                appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun dismiss() {
            if (Companion::dialog.isInitialized && dialog.isShowing) {
                dialog.dismiss()
            }

        }

        //        fun getTwoDigitDate(date: String): String = if (date.length < 2) "0$date" else date
//
//        suspend fun showToast(context: Context, msg: String) {
//            withContext(Dispatchers.Main) {
//                dismiss()
//                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            }
//        }
//
//
//        fun getCatPosition(productsResponse: ProductsResponse): Int {
//            var pos = 0
//            val catList = productsResponse.categories
//
//            for (i in catList.indices) {
//                if (catList[i].id == PowerPreference.getDefaultFile().getInt("catId")) {
//                    pos = i
//                    break
//                }
//
//            }
//            return pos
//        }
//
//
//        fun getGrade(gradeId: Int, grades: List<Grade>): String {
//            var grade = ""
//
//            for (i in grades.indices) {
//                if (gradeId == grades[i].id) {
//                    grade = grades[i].name
//                    break
//                }
//            }
//            return grade
//        }
//
        fun getCategoryIdFromAll(
            item: String?,
            categories: List<Category>
        ): String? {
            var id: Int? = null
            for (i in categories.indices) {
                if (item == categories[i].name) {
                    id = categories[i].id
                    break
                }
            }
            return id.toString() ?: "1"
        }

        fun getSubCategoryIdFromAll(
            item: String?,
            subcategories: List<SubCategory>
        ): String? {
            var id: Int? = null
            for (i in subcategories.indices) {
                if (item == subcategories[i].name) {
                    id = subcategories[i].id
                    break
                }
            }
            return id.toString() ?: "1"
        }

        fun getGradeIdFromAll(
            item: String?,
            gradelist: List<ProductGrade>
        ): String? {
            var id: Int? = null
            for (i in gradelist.indices) {
                if (item == gradelist[i].name) {
                    id = gradelist[i].id
                    break
                }
            }
            return id.toString() ?: "1"
        }

        fun getCategoryId(
            name: String,
            categories: List<Category>
        ): Int {
            var id: Int? = null
            for (i in categories.indices) {
                if (name == categories[i].name) {
                    id = categories[i].id
                }
            }
            return id ?: 0
        }

        //
//        fun getCategoryName(
//            categoryId: Int,
//            categories: List<Category>
//        ): String {
//            var name: String? = null
//            for (i in categories.indices) {
//                if (categoryId == categories[i].id) {
//                    name = categories[i].name
//                    break
//                }
//            }
//            return name ?: ""
//        }
//
//        fun View.snackbar(message: String) {
//            Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
//                snackbar.setAction("Ok") {
//                    snackbar.dismiss()
//                }
//            }.show()
//        }
//
//        fun getGradeId(item: String?, gradeList: MutableList<Grade>): String? {
//            var id = 1
//            for (i in gradeList.indices) {
//                if (item == gradeList[i].name) {
//                    id = gradeList[i].id
//                    break
//                }
//
//            }
//            return id.toString()
//        }
//
        fun getSelectedIndex(
            categoryId: Int,
            allcategoryList: MutableList<Category>
        ): Int {
            var pos = 0
            for (i in allcategoryList.indices) {
                if (categoryId == allcategoryList[i].id) {
                    pos = i
                    break
                }
            }
            return pos
        }

        fun getSelectedIndexforSubcat(
            categoryId: Int,
            allSubcategoryList: MutableList<SubCategory>
        ): Int {
            var pos = 0
            for (i in allSubcategoryList.indices) {
                if (categoryId == allSubcategoryList[i].id) {
                    pos = i
                    break
                }
            }
            return pos
        }

        fun getSelectedIndexforGrade(
            categoryId: Int,
            allGradeList: MutableList<ProductGrade>
        ): Int {
            var pos = 0
            for (i in allGradeList.indices) {
                if (categoryId == allGradeList[i].id) {
                    pos = i
                    break
                }
            }
            return pos
        }


    }
}