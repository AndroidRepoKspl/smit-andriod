package com.kspl.smitglobal.utils

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.kspl.smitglobal.R
import com.skydoves.powermenu.CircularEffect
import com.skydoves.powermenu.MenuAnimation
import com.skydoves.powermenu.PowerMenu
import com.skydoves.powermenu.PowerMenuItem

class MorMenuForCustomer: PowerMenu.Factory() {
    override fun create(context: Context, lifecycle: LifecycleOwner): PowerMenu {

        return PowerMenu.Builder(context)
            //   addItem(PowerMenuItem("All Data", R.drawable.ic_baseline_chat_bubble_outline_24,true))
            .addItem(PowerMenuItem("Sync",R.drawable.ic_baseline_sync_24,false))
            .addItem(PowerMenuItem("Update Profile",R.drawable.ic_baseline_account_box_24,false))
            .addItem(PowerMenuItem("View Enquiries",R.drawable.ic_baseline_assignment_24,false))
            .addItem(PowerMenuItem("View Brochure",R.drawable.ic_baseline_assignment_24,false))
            .addItem(PowerMenuItem("Logout",R.drawable.ic_baseline_power_settings_new_24, false))
            .setAutoDismiss(true)
            .setLifecycleOwner(lifecycle)
            .setTextColor(ContextCompat.getColor(context, R.color.blue))
            .setTextSize(14)
            .setWidth(600)
            .setCircularEffect(CircularEffect.BODY) // shows circular revealed effects for all body of the popup menu.
            .setMenuColor(Color.WHITE)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setAnimation(MenuAnimation.FADE)
            //  .setInitializeRule(Lifecycle.Event.ON_CREATE, 0)
            .build()

    }
}