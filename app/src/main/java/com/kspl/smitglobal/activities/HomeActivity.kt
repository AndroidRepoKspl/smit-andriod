package com.kspl.smitglobal.activities

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityHomeBinding
import com.kspl.smitglobal.databinding.DialogUpdateProfileBinding
import com.kspl.smitglobal.modelClasses.UpdateUserDetils.UpdateUserResponse
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeActivity : BaseActivity() {

    lateinit var binding: ActivityHomeBinding;

    lateinit var updateProfileDialog: AlertDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
            binding.cvAppManage.visibility = View.GONE
            binding.cvAboutUs.visibility =View.VISIBLE
            binding.cvContactUs.visibility =View.VISIBLE

        } else {
            binding.cvAppManage.visibility = View.VISIBLE
            binding.cvAboutUs.visibility =View.GONE
            binding.cvContactUs.visibility =View.GONE
        }

        binding.ivProfile.setOnClickListener {
            openUpdateProfileDialog()
        }
        binding.cvAppManage.setOnClickListener {
            startActivity(Intent(bContext, CategryActivity::class.java))
        }
        binding.cvOurCategories.setOnClickListener {
            startActivity(Intent(bContext, CategoryActivity::class.java))
        }

        binding.cvViewBrochure.setOnClickListener {
            startActivity(Intent(bContext, ViewFilesActivity::class.java))
        }

        binding.cvViewEnquiries.setOnClickListener {

            if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
                startActivity(Intent(bContext, SendToEnquiryActivity::class.java))
            } else {
                startActivity(Intent(this, UsersActivity::class.java))
            }


        }

        binding.cvAboutUs.setOnClickListener {
            startActivity(Intent(bContext, AboutUsActivity::class.java))
        }

        binding.cvContactUs.setOnClickListener {
            startActivity(Intent(bContext, ContactUsActivity::class.java))
        }




    }

    override fun onBackPressed() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Exit ?")
        builder.setMessage("Are you sure you want to exit?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                finish()
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun openUpdateProfileDialog() {
        val dialogBinding: DialogUpdateProfileBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(bContext),
                R.layout.dialog_update_profile, null, false
            )
        updateProfileDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.etName.setText(PowerPreference.getDefaultFile().getString("name"))
        dialogBinding.etEmail.setText(PowerPreference.getDefaultFile().getString("email"))
        dialogBinding.etPhoneNum.setText(PowerPreference.getDefaultFile().getString("contact_no"))


        dialogBinding.cancel.setOnClickListener {
            updateProfileDialog.dismiss()
        }

        dialogBinding.logout.setOnClickListener {
            updateProfileDialog.dismiss()
            logout()
        }

        dialogBinding.update.setOnClickListener {

            var userid = PowerPreference.getDefaultFile().getString("userid")
            var name = dialogBinding.etName.text.toString()
            var email = dialogBinding.etEmail.text.toString()

            if (name.isNullOrBlank()) {
                dialogBinding.etName.requestFocus()
                dialogBinding.etName.error = "Name cannot be empty"
            } else if (email.isNullOrBlank()) {
                dialogBinding.etEmail.requestFocus()
                dialogBinding.etEmail.error = "Email cannot be empty"
            } else {
                Constants.showProgressDialog(bContext, "Updating Profile", "Please Wait")

                CoroutineScope(Dispatchers.IO).launch {
                    if (Constants.isNetworkAvailable()) {
                        try {
                            val response =
                                ApiClient.apiService.updateProfile(
                                    Constants.getHeadersWithToken(),
                                    userid,
                                    name,
                                    "",
                                    email
                                )
                            if (response.isSuccessful) {
                                if (response.body() != null) {
                                    val data: UpdateUserResponse? = response.body()
                                    sendMsg(data!!.message)
                                    PowerPreference.getDefaultFile().apply {
                                        setString("name", name)
                                        setString("email", email)
                                    }

                                } else {
                                    sendMsg(Constants.NULL_RESPONSE)
                                }
                            } else {
                                sendMsg(response.message())
                            }

                        } catch (e: Exception) {
                            sendMsg(e.message!!)
                        }
                    } else {
                        sendMsg(Constants.NO_INTERNET)
                    }
                }
            }


        }
    }

    private suspend fun sendMsg(msg: String) {
        withContext(Dispatchers.Main) {
            Constants.dismiss()
            Toast.makeText(bContext, msg, Toast.LENGTH_SHORT).show()
            if (::updateProfileDialog.isInitialized) {
                updateProfileDialog.dismiss()
            }
        }
    }

    private fun logout() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to Logout?")
            .setCancelable(false)
            .setPositiveButton("yes") { dialog, id ->
                clearPref()
                clearPref()
                val intent = Intent(bContext, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }
}