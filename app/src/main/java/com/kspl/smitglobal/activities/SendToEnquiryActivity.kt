package com.kspl.smitglobal.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.EnquiryAdapter
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivitySendToEnquiryBinding
import com.kspl.smitglobal.databinding.DialogUpdateProfileBinding
import com.kspl.smitglobal.modelClasses.UpdateUserDetils.UpdateUserResponse
import com.kspl.smitglobal.modelClasses.deleteEnquiry.DeleteEnquiryResponse
import com.kspl.smitglobal.modelClasses.getEnquiries.DataX
import com.kspl.smitglobal.modelClasses.getEnquiries.GetEnquiresResponse
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.Serializable

class SendToEnquiryActivity : BaseActivity(), EnquiryAdapter.EnquiryItemClick {
    private lateinit var binding: ActivitySendToEnquiryBinding
    private lateinit var context: Context
    lateinit var updateProfileDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_to_enquiry)
        context = this

        getEnquiryList()

        PowerPreference.init(context)

        binding.ivHistory.setOnClickListener {
            val intent = Intent(bContext,UserEnquiryListActivity::class.java)
            intent.putExtra("id",PowerPreference.getDefaultFile().getString("userid"))
            startActivity(intent)
        }

        binding.ivMenu.setOnClickListener {
            val intent = Intent()
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        binding.btnSubmitEnquiry.setOnClickListener {


            if (PowerPreference.getDefaultFile().getString("name") == "") {
                updateName()
            } else {
                submitEnquiry()
            }

        }

    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun updateName() {
        val dialogBinding: DialogUpdateProfileBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.dialog_update_profile, null, false
            )
        updateProfileDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.logout.visibility = View.GONE
        dialogBinding.etName.setText(PowerPreference.getDefaultFile().getString("name"))
        dialogBinding.etEmail.setText(PowerPreference.getDefaultFile().getString("email"))
        dialogBinding.etPhoneNum.setText(PowerPreference.getDefaultFile().getString("contact_no"))


        dialogBinding.cancel.setOnClickListener {
            updateProfileDialog.dismiss()
        }

        dialogBinding.update.setOnClickListener {

            var userid = PowerPreference.getDefaultFile().getString("userid")
            var name = dialogBinding.etName.text!!.trim().toString()
            var email = dialogBinding.etEmail.text!!.trim().toString()


            if (name.isNullOrBlank()) {
                dialogBinding.etName.requestFocus()
                dialogBinding.etName.error = "Name cannot be empty"
            } else if (email.isNullOrBlank()) {
                dialogBinding.etEmail.requestFocus()
                dialogBinding.etEmail.error = "Email cannot be empty"
            }else{
                Constants.showProgressDialog(context, "Updating Profile", "Please Wait")

                CoroutineScope(Dispatchers.IO).launch {
                    if (Constants.isNetworkAvailable()) {
                        try {
                            val response =
                                ApiClient.apiService.updateProfile(
                                    Constants.getHeadersWithToken(),
                                    userid,
                                    name,
                                    "",
                                    email
                                )
                            if (response.isSuccessful) {
                                if (response.body() != null) {
                                    val data: UpdateUserResponse? = response.body()
                                    sendMsg(data!!.message)
                                    PowerPreference.getDefaultFile().apply {
                                        setString("name", name)
                                        setString("email", email)

                                    }

                                } else {
                                    sendMsg(Constants.NULL_RESPONSE)
                                }
                            } else {
                                sendMsg(response.message())
                            }

                        } catch (e: Exception) {
                            sendMsg(e.message!!)
                        }
                    } else {
                        sendMsg(Constants.NO_INTERNET)
                    }
                }
            }


        }
    }

    private suspend fun sendMsg(msg: String) {
        withContext(Dispatchers.Main) {
            Constants.dismiss()
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            if (::updateProfileDialog.isInitialized) {
                updateProfileDialog.dismiss()
                submitEnquiry()
            }
        }
    }

    private fun submitEnquiry() {
        Constants.showProgressDialog(context, "Submitting Enquiry", "Please Wait")
        CoroutineScope(Dispatchers.Main).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.submitEnquiry(
                            Constants.getHeadersWithToken()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: DeleteEnquiryResponse? = response.body()
                            if (data!!.status == Constants.SUCCESS) {
                                sendMsgToMainThread(data.message)
                                val intent = Intent()
                                setResult(RESULT_OK, intent)
                                finish()
                            } else {
                                sendMsgToMainThread(data.message)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }


    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(context, it, Toast.LENGTH_LONG).show() }

            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getEnquiryList() {
        Constants.showProgressDialog(context, "Getting Enquiry List", "Please Wait")
        CoroutineScope(Dispatchers.Main).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllEnquiries(
                            Constants.getHeadersWithToken()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: GetEnquiresResponse? = response.body()
                            if (data!!.status == Constants.SUCCESS) {
                                val list = data.data
                                dissmissDialog()
                                if (list.isNotEmpty()) {
                                    binding.rvEnquiry.visibility = View.VISIBLE
                                    binding.tvNoData.visibility = View.GONE
                                    setDatatoRv(list)
                                } else {
                                    binding.rvEnquiry.visibility = View.GONE
                                    binding.tvNoData.visibility = View.VISIBLE
                                }

                            } else {
                                sendMsgToMainThread(data.message)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }

    private fun setDatatoRv(list: List<DataX>) {
        if (list.isNotEmpty()) {
            binding.btnSubmitEnquiry.visibility = View.VISIBLE
        }
        binding.rvEnquiry.layoutManager = LinearLayoutManager(this)
        binding.rvEnquiry.setHasFixedSize(true)
        binding.rvEnquiry.adapter = EnquiryAdapter(list, this@SendToEnquiryActivity)
    }

    override fun onEnquirySend(product: DataX) {


        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Delete ${product.name} ?")
        builder.setMessage("Are you sure you want to delete ${product.name}")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                deleteProductEnquiry(product.id)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()

    }

    private fun deleteProductEnquiry(id: Int) {

        Constants.showProgressDialog(context, "Deleting Product", "Please Wait")
        CoroutineScope(Dispatchers.Main).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.deleteEnquiryProduct(
                            Constants.getHeadersWithToken(), id
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: DeleteEnquiryResponse? = response.body()
                            if (data!!.status == Constants.SUCCESS) {
                                dissmissDialog()
                                getEnquiryList()
                            } else {
                                sendMsgToMainThread(data.message)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }


}