package com.kspl.smitglobal.activities

import android.content.Intent
import android.content.IntentFilter
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient.Companion.apiService
import com.kspl.smitglobal.databinding.ActivityLoginBinding
import com.kspl.smitglobal.modelClasses.loginWithOtp.OtpResponse
import com.kspl.smitglobal.modelClasses.loginWithPassword.LoginWithPassword
import com.kspl.smitglobal.utils.Constants
import com.kspl.smitglobal.utils.Constants.Companion.SUCCESS
import com.kspl.smitglobal.utils.Constants.Companion.getHeadersWithoutToken
import com.kspl.smitglobal.utils.Constants.Companion.isNetworkAvailable
import com.kspl.smitglobal.utils.sms.AppSignatureHashHelper
import com.kspl.smitglobal.utils.sms.SMSReceiver
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginActivity : BaseActivity(), SMSReceiver.OTPReceiveListener  {

    lateinit var binding:ActivityLoginBinding
    var isOtpView = true
    private val TAG: String = LoginActivity::class.java.simpleName
    private  var smsReceiver:SMSReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PowerPreference.getDefaultFile().getBoolean("isLogin")) {

            CoroutineScope(Main).launch {
                sendToActivity()
            }
        }
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        val appSignatureHashHelper = AppSignatureHashHelper(this)

        Log.i(TAG, "HashKey: " + appSignatureHashHelper.appSignatures[0])
        // LXqD1KaMpHC
        startSMSListener()

        setOtpView()
        binding.btnGetOtp.setOnClickListener {

          //  showVirificationOtpView()
            validateContactNumber()

         //   showdilog()
        }

        binding.btnVerifyOtp.setOnClickListener {
            verifyOtp()
        }

        binding.tvResendOtp.setOnClickListener {
            validateContactNumber() 
        }

        binding.tvLoginWithPassword.setOnClickListener {
            if(isOtpView){
                isOtpView = false
                setLoginWithPasswordView()
            }else
            {
                isOtpView = true
                setOtpView()
            }
        }

        binding.btnLogin.setOnClickListener {
            validate()
        }

    }

    private fun verifyOtp() {
        val number = binding.etContactNumber.text.toString().trim()
        val otp = binding.etOtpCode.text.toString().trim()

        if(number.length!=10)
        {
            binding.etContactNumber.error = "Number should be in 10 digits"
            binding.etContactNumber.requestFocus()
        }else if(otp.length!=6)
        {
            binding.etOtpCode.error = "Otp should be 6 digits"
            binding.etOtpCode.requestFocus()
        }else{
            CoroutineScope(Main).launch {
                sendToVerifyOtp(number, otp)
            }
        }
    }

    private suspend fun sendToVerifyOtp(mobileNo: String, otp: String) {
        Constants.showProgressDialog(bContext, "Verifying OTP", "Please Wait")
             CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = apiService.verify(getHeadersWithoutToken(),null,
                mobileNo,otp,null,null)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if(response.body()!!.status==SUCCESS){
                            saveUserData(response.body()!!)
                            sendToActivity()
                        }else
                        {
                            response.body()!!.message?.let { sendMsg(it) }
                        }
                    } else {
                        sendMsg("Something Went Wrong")
                    }
                } else {
                    sendMsg(response.message())
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message.toString())
            }
        }

    }

    private fun validateContactNumber() {
        val number = binding.etContactNumber.text.toString().trim()
        if(number.length!=10){
            binding.etContactNumber.requestFocus()
            binding.etContactNumber.error = "Invalid Mobile Number"
        }else{
            callLoginWithOtp(number)
        }
    }

    private fun callLoginWithOtp(number: String) {

        Constants.showProgressDialog(bContext, "Getting OTP", "Please Wait")
        if (isNetworkAvailable()) {

        lifecycleScope.launch(Dispatchers.Main) {
            try {
                val response = apiService.getOtp(
                    getHeadersWithoutToken(),
                    number
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (response.body()!!.status == SUCCESS) {
                            handleResponse(response.body())
                        } else {
                            sendMsg(response.body()!!.message)
                        }
                    } else {
                        sendMsg("Something Went Wrong")
                    }
                } else {
                    sendMsg(response.message())
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message.toString())
            }
        }
    }else{
            sendMsg("No Internet Connection")
        }
    }

    private suspend fun handleResponse(response: OtpResponse?) {
        withContext(Main){
            dissmissDialog()
            showVirificationOtpView()
        }

    }


    private fun startSMSListener() {
        try {
            smsReceiver = SMSReceiver()
            smsReceiver!!.setOTPListener(this)
            val intentFilter = IntentFilter()
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
            this.registerReceiver(smsReceiver, intentFilter)
            val client = SmsRetriever.getClient(this)
            val task = client.startSmsRetriever()
            task.addOnSuccessListener {
                // API successfully started
            }
            task.addOnFailureListener {
                // Fail to start API
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun validate() {
        val email = binding.etContactNumber.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()

        when {
            email.isEmpty() -> {
                binding.etContactNumber.requestFocus()
                binding.etContactNumber.error = "please enter Contact Number"
            }
            password.isEmpty() -> {
                binding.etPassword.requestFocus()
                binding.etPassword.error = "Please Enter Password"
            }
            else -> {
                lifecycleScope.launch(Dispatchers.Main) {
                    login(email, password)
                }

            }
        }
    }

    private fun login(email: String, password: String) {
        Constants.showProgressDialog(bContext, "Logging in", "Please Wait")
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = apiService.loginWithPassword(getHeadersWithoutToken(),
                    email,password)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                       if(response.body()!!.status==SUCCESS){
                           saveUserData(response.body()!!)
                           sendToActivity()
                       }else
                       {
                           response.body()!!.message?.let { sendMsg(it) }
                       }
                    } else {
                        sendMsg("Something Went Wrong")

                    }
                } else {
                    sendMsg(response.message())
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message.toString())
            }
        }
    }

    private suspend fun sendToActivity() {
        withContext(Main){
            if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
                val intent = Intent(bContext, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                val intent = Intent(bContext, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
    }

    private suspend fun saveUserData(response: LoginWithPassword) {
        withContext(Main){
            dissmissDialog()
        }
        PowerPreference.getDefaultFile().apply {
            setBoolean("isLogin", true)
            setString("userid", response.user.id.toString())
            setString("name", response.user.name)
            setString("email", response.user.email)
            setString("contact_no", response.user.mobile_no)
            setString("role_id", response.user.role_id.toString())
            setString("token",response.data.access_token)

        }
    }

    private fun sendMsg(msg: String) {
        CoroutineScope(Main).launch {
            dissmissDialog()
            toast(msg)
        }
    }

    private fun setLoginWithPasswordView() {
        binding.etContactNumber.visibility = View.VISIBLE
        binding.tvLoginWithPassword.text = "Login With OTP?"
        binding.btnGetOtp.visibility = View.GONE
        binding.etOtpCode.visibility= View.GONE
        binding.btnVerifyOtp.visibility= View.GONE
        binding.tvResendOtp.visibility=View.GONE
        binding.editTextPassword.visibility = View.VISIBLE
        binding.btnLogin.visibility = View.VISIBLE



    }

    private fun setOtpView() {
        binding.editTextPassword.visibility = View.GONE
        binding.btnGetOtp.visibility = View.VISIBLE
        binding.etOtpCode.visibility = View.GONE
        binding.btnVerifyOtp.visibility = View.GONE
        binding.btnLogin.visibility = View.GONE
        binding.tvResendOtp.visibility = View.GONE
        binding.tvLoginWithPassword.visibility = View.VISIBLE
        binding.tvLoginWithPassword.text = "Login With Password?"
    }







    private fun showdilog() {

    }

    private fun showVirificationOtpView() {
        binding.etContactNumber.visibility= View.VISIBLE
        binding.btnGetOtp.visibility= View.GONE
        binding.etOtpCode.visibility= View.VISIBLE
        binding.btnVerifyOtp.visibility= View.VISIBLE
        binding.tvResendOtp.visibility=View.VISIBLE
        binding.tvMessage.text = "Thank you for connecting with SMIT. \nEnter OTP code that we have sent on your phone"
    }

    override fun onOTPReceived(otp: String?) {
        binding.etOtpCode.setText(otp?.substring(26,32))
        if (smsReceiver != null) {
            unregisterReceiver(smsReceiver)
            smsReceiver = null
        }
        verifyOtp()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (smsReceiver != null) {
            unregisterReceiver(smsReceiver)
        }
    }

    override fun onOTPTimeOut() {

    }

    override fun onOTPReceivedError(error: String?) {

    }

}