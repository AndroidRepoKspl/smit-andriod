package com.kspl.smitglobal.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.MultipleSelectActivity
import com.kspl.smitglobal.ProductAdapter
import com.kspl.smitglobal.ProductClickListener
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.api.ApiClient.Companion.IMG_URL
import com.kspl.smitglobal.databinding.ActivityProductBinding
import com.kspl.smitglobal.databinding.DeleteGradeDialogBinding
import com.kspl.smitglobal.databinding.DetailDialogLayoutBinding
import com.kspl.smitglobal.modelClasses.addEnquiry.AddEnquiryResponse
import com.kspl.smitglobal.modelClasses.products.Product
import com.kspl.smitglobal.modelClasses.products.ProductsResponse
import com.kspl.smitglobal.utils.Constants
import com.kspl.smitglobal.utils.Constants.Companion.SUCCESS
import com.kspl.smitglobal.utils.Constants.Companion.isSelectionModeOn
import com.preference.PowerPreference
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.Serializable

class ProductActivity : BaseActivity(), ProductClickListener,
    ProductAdapter.deleteProduct, ProductAdapter.editProduct,
    SearchView.OnQueryTextListener,
        ProductAdapter.ProductDetails
{
    var name: String? = null
    var id: String? = null
    lateinit var binding: ActivityProductBinding
    lateinit var context: Context
    private var productList: List<Product> = ArrayList()
    private var adapter: ProductAdapter? = null
    private var allProductList: List<Product> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product)
        context = this


        if (intent.hasExtra("name")) {
            name = intent.getStringExtra("name")
            binding.textView.text = name
        }
        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id")
        }
        // productList = getList()
        binding.ivMenu.setOnClickListener { finish() }

        if (PowerPreference.getDefaultFile().getString("role_id") == "2") {
            binding.btnSelectionMode.visibility = View.GONE
        } else {
            binding.btnSelectionMode.visibility = View.VISIBLE
        }

        if (PowerPreference.getDefaultFile().getString("role_id") == "2") {
            binding.ivFav.visibility = View.GONE
        } else {
            binding.ivFav.visibility = View.VISIBLE
        }
        binding.ivSync.setOnClickListener {
            getProducts()
        }

        binding.ivFav.setOnClickListener {
            var intent = Intent(bContext, SendToEnquiryActivity::class.java)
            startActivityForResult(intent, 222)
        }



        getProducts()

        binding.searchView4.isIconified = false
        binding.searchView4.onActionViewExpanded()

        binding.searchView4.isFocusable = true
        binding.searchView4.requestFocusFromTouch()
        binding.searchView4.clearFocus()
        binding.searchView4.setOnQueryTextListener(this)
        binding.searchView4.setOnClickListener { binding.searchView4.isIconified = false }

        val id = binding.searchView4.context.resources.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = binding.searchView4.findViewById(id) as TextView
        textView.setTextColor(Color.WHITE)
        textView.setHintTextColor(resources.getColor(R.color.white))


        updateSearchList(object : SearchList {
            override fun search(text: String) {
                val tempList: MutableList<Product> = ArrayList()
                val list = getList()
                for (i in list.indices) {
                    if (list[i].name.toLowerCase().contains(text.toLowerCase())) {
                        tempList.add(list[i])
                    }
                }
                binding.rvCategories.layoutManager = LinearLayoutManager(context)
                binding.rvCategories.adapter = ProductAdapter(
                    tempList, this@ProductActivity,
                    this@ProductActivity, this@ProductActivity,
                    this@ProductActivity
                )
            }
        })

        binding.btnSelectionMode.setOnClickListener { view ->
            if (isSelectionModeOn) {
                clearSelectionMode()

            } else {
                isSelectionModeOn = true
                binding.btnSelectionMode.setImageDrawable(resources.getDrawable(R.drawable.select_on))
                adapter?.updateList(getList())
            }
        }
        binding.btnSend.setOnClickListener { view ->
            val selectedProductList =
                adapter!!.selectedProducts
            val builder = StringBuilder()
            for (i in selectedProductList.indices) {
                if (i == 0) {
                    builder.append(selectedProductList[i].id)
                } else {
                    builder.append(",").append(selectedProductList[i].id)
                }
            }

       //     toast(builder.toString())


            addToEnquiry(builder.toString())


            /*  val enquiryList: MutableList<Product> = ArrayList<Product>()

              val enquiryArray: Array<Product>? = Gson().fromJson(
                  PowerPreference.getDefaultFile().getString("enquiry"),
                  Array<Product>::class.java
              )

              if(enquiryArray !=null){
                  if(enquiryArray.isNotEmpty()){
                      for (c in enquiryArray) {
                          enquiryList.add(c)
                      }
                  }
              }
               enquiryList.addAll(adapter!!.selectedProducts)
              PowerPreference.getDefaultFile().putString("enquiry", Gson().toJson(enquiryList))
             */

        }

    }

    private fun addToEnquiry(products: String) {
        Constants.showProgressDialog(context, "Adding Products for Enquiry", "Please Wait")
        CoroutineScope(Dispatchers.Main).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.addEnquiry(
                            Constants.getHeadersWithToken(),
                            products
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: AddEnquiryResponse? = response.body()
                            if (data!!.status != null) {
                                if (data.status == SUCCESS) {
                                    sendMsgToMainThread(data.message)
                                    clearSelectionMode()
                                    withContext(Main) {
                                        getProducts()
                                    }

                                } else {
                                    sendMsgToMainThread(data.message)
                                }
                            } else {
                                sendMsgToMainThread(data.message)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }

    private fun clearSelectionMode() {
        isSelectionModeOn = false
        binding.btnSelectionMode.setImageDrawable(resources.getDrawable(R.drawable.select_off))
        binding.btnSend.visibility = View.GONE
        adapter?.updateList(getList())

    }

    private fun getList(): List<Product> {
        val enquiryList: MutableList<Product> = ArrayList<Product>()
        val enquiryArray: Array<Product>? = Gson().fromJson(
            PowerPreference.getDefaultFile().getString("list"),
            Array<Product>::class.java
        )
        if (enquiryArray != null) {
            if (enquiryArray.isNotEmpty()) {
                for (c in enquiryArray) {
                    enquiryList.add(c)
                }
            }
        }

        return enquiryList
    }

    private fun getProducts() {
        Constants.showProgressDialog(context, "Loading products", "Please Wait")
        CoroutineScope(Main).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getroducts(
                            Constants.getHeadersWithToken(),
                            id
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            Constants.dismiss()
                            val data: ProductsResponse? = response.body()
                            PowerPreference.getDefaultFile()
                                .putString("list", Gson().toJson(data!!.products))
                            if (data.products.size > 0) {
                                withContext(Main) {
                                    binding.tvNoDataFound.visibility = View.GONE
                                    binding.rvCategories.visibility = View.VISIBLE
                                    setDataToRv( data!!.products)
                                }
                            } else {
                                withContext(Main) {
                                    dissmissDialog()
                                    binding.tvNoDataFound.visibility = View.VISIBLE
                                    binding.rvCategories.visibility = View.GONE

                                }
                            }


                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private suspend fun setDataToRv(products: List<Product>) {

        withContext(Main) {
            binding.rvCategories.layoutManager = LinearLayoutManager(this@ProductActivity)
            binding.rvCategories.setHasFixedSize(true)
            adapter = ProductAdapter(
                products,
                this@ProductActivity,
                this@ProductActivity,
                this@ProductActivity,
                this@ProductActivity
            )
            binding.rvCategories.adapter = adapter
        }


    }

    override fun onBackPressed() {
        if (isSelectionModeOn) {
            clearSelectionMode()
        } else {
            super.onBackPressed()
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(context, it, Toast.LENGTH_LONG).show() }

            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    /*  private fun getList(): List<Product> {
          val list: MutableList<Product> = ArrayList()
          for (i in 0..9) {
              list.add(Product("product $i", "$i", false))
          }
          return list
      }*/

    override fun onProductClickAction(isSelected: Boolean?) {
        if (isSelected!!) {
            binding.btnSend.visibility = View.VISIBLE
            MultipleSelectActivity.isSelectionModeOn = true
            binding.btnSelectionMode.setImageDrawable(resources.getDrawable(R.drawable.select_on))
        } else {
            binding.btnSend.visibility = View.GONE
        }
    }

    override fun deleteProductClick(product: Product) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Delete ${product.name} ?")
        builder.setMessage("Are you sure you want to delete this Product?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                deleteProduct(product)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun deleteProduct(product: Product) {

        Constants.showProgressDialog(
            context,
            "Deleting Product", "Please Wait..."
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response =
                    ApiClient.apiService.deleteProduct(Constants.getHeadersWithToken(), product.id)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        deleteCategoryMsg("Successfully Deleted")
                    } else {
                        deleteCategoryMsg("Something went wrong")
                    }
                } else {
                    deleteCategoryMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                deleteCategoryMsg(e.message)
            }
        }

    }

    private suspend fun deleteCategoryMsg(msg: String?) {
        withContext(Main) {
            Constants.dismiss()
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            getProducts()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 222) {
            if (resultCode == RESULT_OK) {
                getProducts()
            }
        }
    }

    override fun editProductClick(product: Product) {
        var intent = Intent(bContext, UpdateProductActivity::class.java)
        intent.putExtra("product", product as Serializable)
        startActivityForResult(intent, 222)
    }

    override fun onImageClicked(product: Product?) {
        if (product?.multiple_images != null) {
            if (product.multiple_images.isNotEmpty()) {
                val intent = Intent(bContext, ImageViewActivity::class.java)
                intent.putExtra("product", product as Serializable)
                startActivity(intent)
            } else {
                val intent = Intent(bContext, ImageViewActivity::class.java)
                intent.putExtra("product", product as Serializable)
                startActivity(intent)
            }
        } else {
            toast("No Images Found")
        }
    }

    override fun onEnquiryClick(product: Product?) {
        addToEnquiry(product?.id.toString())
    }

    override fun onDeleteClick(product: Product?) {
    }

    override fun onQueryTextSubmit(s: String?): Boolean = false

    override fun onQueryTextChange(s: String?): Boolean {
        searchList.search(s!!)
        return false
    }

    private lateinit var searchList: SearchList
    fun updateSearchList(listener: SearchList) {
        searchList = listener

    }

    interface SearchList {
        fun search(text: String)
    }

    override fun onStop() {
        super.onStop()
        clearSelectionMode()
    }

    override fun productDetailsClick(product: Product) {
        val dialogBinding: DetailDialogLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.detail_dialog_layout, null, false
        )
       var detailDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(true)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.ivCross.setOnClickListener { detailDialog.dismiss() }

        dialogBinding.tvName.text = product.name
        dialogBinding.tvCatName.text = "Category: "+product.product_category
        dialogBinding.tvSubcatName.text = "Sub-Categoty Name: "+product.sub_category_name
        if(product.company_name!=null){
            dialogBinding.tvCompName.text = "Company Name: "+product.company_name
        }else
        {
            dialogBinding.tvCompName.visibility = View.GONE
        }
        dialogBinding.tvGrade.text = "Grade: "+product.product_grade
        dialogBinding.tvPriceUnit.text =  "\u20B9${product!!.price} / (${product.unit} Unit)"
        if(product.description!=null){
            dialogBinding.tvDesc.text = product.description
        }else
        {
            dialogBinding.tvDesc.visibility = View.GONE
        }
        Picasso.get()
            .load(IMG_URL+product.multiple_images[0])
            .error(R.drawable.square_logo)
            .placeholder(R.drawable.square_logo)
            .into(dialogBinding.ivItemImage)


    }
}