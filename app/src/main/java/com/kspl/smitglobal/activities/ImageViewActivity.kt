package com.kspl.smitglobal.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.ViewImagesAdapter
import com.kspl.smitglobal.adaptors.ViewpagerAdapter
import com.kspl.smitglobal.databinding.ActivityImageViewBinding
import com.kspl.smitglobal.modelClasses.products.Product
import java.lang.Exception

class ImageViewActivity : AppCompatActivity(),ViewImagesAdapter.ImageSelect,ViewpagerAdapter.Clicked {
    private var isvisible  = true
    private lateinit var binding: ActivityImageViewBinding
    lateinit var  product : Product
    private lateinit var imagesArray: MutableList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_image_view)

        product = intent.getSerializableExtra("product") as Product

        binding.backImageView.setOnClickListener { finish() }

        imagesArray = product.multiple_images as MutableList<String>

        if(imagesArray.isNotEmpty()){
            binding.vpProductImage.adapter = ViewpagerAdapter(imagesArray,this@ImageViewActivity)
            binding.vpProductImage.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            binding.rvImages.setHasFixedSize(true)
            binding.rvImages.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            binding.rvImages.setItemViewCacheSize(imagesArray.size)
            binding.rvImages.adapter = ViewImagesAdapter(imagesArray,this@ImageViewActivity)
        }else
        {
            val string: String = product.file_path
            try {
                val imageList: List<String> = string.split("uploads/")
                val imageName = imageList[1]
                imagesArray.add(imageName)
                binding.vpProductImage.adapter = ViewpagerAdapter(imagesArray,this@ImageViewActivity)
                binding.vpProductImage.orientation = ViewPager2.ORIENTATION_HORIZONTAL
                binding.rvImages.setHasFixedSize(true)
                binding.rvImages.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                binding.rvImages.setItemViewCacheSize(imagesArray.size)
                binding.rvImages.adapter = ViewImagesAdapter(imagesArray,this@ImageViewActivity)
            }catch (e: Exception){

            }

        }
    }

    override fun onImageSelect(pos: Int) {
        binding.vpProductImage.setCurrentItem(pos,true)
    }

    override fun onClicked() {
        if(!isvisible){
            binding.rvImages.visibility = View.VISIBLE
            isvisible = true
        }else
        {
            binding.rvImages.visibility = View.GONE
            isvisible = false
        }
    }
}