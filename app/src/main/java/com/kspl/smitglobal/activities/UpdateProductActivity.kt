package com.kspl.smitglobal.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jaredrummler.materialspinner.MaterialSpinner
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.ImagesAdapter
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityAddProductBinding
import com.kspl.smitglobal.databinding.ActivityUpdateProductBinding
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.modelClasses.GradeResponse.GetGradeResponse
import com.kspl.smitglobal.modelClasses.GradeResponse.ProductGrade
import com.kspl.smitglobal.modelClasses.ImageModel.ImageModel
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategory
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategoryResponse
import com.kspl.smitglobal.modelClasses.products.Category
import com.kspl.smitglobal.modelClasses.products.Product
import com.kspl.smitglobal.utils.CompressImage
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.*
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class UpdateProductActivity : BaseActivity(), ImagesAdapter.deleteProductImage {

    lateinit var product: Product
    var selectedUnit: String? = null
    var selectedCategory: String? = null
    var selectedSubCategory: String? = null
    var selectedGrade: String? = null

    var productName: String? = null
    var description: String? = null
    var companyName: String? = null

    var price: String? = null
    var selectedCountry: String? = ""

    val REQUEST_ID_MULTIPLE_PERMISSIONS = 101
    val MY_PERMISSIONS_REQUEST_CAMERA = 1
    val MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 2
    val MY_PERMISSIONS_READ_EXTERNAL_STORAGE = 3
    val SELECT_FILE = 1

    val REQUEST_CAMERA = 0
    var destination: File? = null
    private val client = OkHttpClient()
    var imgName: String? = null

    var serResponse = ""
    var spinnerCategorylist: MutableList<String> = ArrayList()
    var spinnerSubCategorylist: MutableList<String> = ArrayList()
    var spinnerCountrylist: MutableList<String> = ArrayList()
    var spinnerGradelist: MutableList<String> = ArrayList()

    private var allCategories: List<com.kspl.smitglobal.modelClasses.CategoryResponse.Category> =
        ArrayList()
    private var allSubCategories: List<SubCategory> = ArrayList()
    private var allGradeList: List<ProductGrade> = ArrayList()

    lateinit var imageAdapter: ImagesAdapter
    var layoutManager: LinearLayoutManager? = null
    var imageNames: MutableList<String> = ArrayList()
    var apiImages: MutableList<String> = ArrayList()

    lateinit var binding: ActivityUpdateProductBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_product)

        product = intent.getSerializableExtra("product") as Product
        apiImages = product.multiple_images as MutableList<String>
        imageAdapter = ImagesAdapter(this, this@UpdateProductActivity)

        binding.ivBack.setOnClickListener {
            closeActivity()
            imageAdapter.selectedImages.clear()
        }

        setAllData(product)

        binding.tvTakePicture.setOnClickListener {
            if (checkAndRequestPermissions(this)) {
                if (imageAdapter.selectedImages.size < 3) {
                    showCameraGalaryDialog()
                } else {
                    Toast.makeText(bContext, "You can add only three pictures", Toast.LENGTH_SHORT)
                        .show()
                }

            }
        }

        binding.spinnerSubCategory.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                if (allSubCategories.size != 0) {
                    selectedSubCategory = Constants.getSubCategoryIdFromAll(item, allSubCategories)
                }
            })

        binding.spinnerGrade.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                if (allGradeList.size != 0) {
                    selectedGrade = Constants.getGradeIdFromAll(item, allGradeList)
                }
            })


    }

    private fun setAllData(product: Product) {
        for (i in apiImages) {
            val imageModel = ImageModel()
            imageModel.imageLocation = "API_IMAGE_${i}"
            imageModel.imageName = i
            imageAdapter.addItem(imageModel)
        }
        binding.rvImages.visibility = View.VISIBLE
        layoutManager = LinearLayoutManager(this)
        layoutManager!!.setOrientation(RecyclerView.HORIZONTAL)
        binding.rvImages.setAdapter(imageAdapter)
        binding.rvImages.setLayoutManager(layoutManager)
        binding.rvImages.setHasFixedSize(true)

        binding.etProductName.setText(product.name)
        binding.etProductDesc.setText(product.description)
        binding.etCompanyName.setText(product.company_name)
        binding.etPrice.setText(product.price)
        binding.spinnerCountry.setText(product.country_name)

        binding.spinnerCategory.hint = product.product_category
        binding.spinnerSubCategory.hint = product.sub_category_name
        binding.spinnerGrade.hint=product.product_grade
        binding.spinnerQuatity.hint=product.unit

        selectedUnit = product.unit
        selectedCountry=product.country_name
        selectedGrade=product.grade_id.toString()
        selectedCategory=product.category_id.toString()
        selectedSubCategory=product.sub_category_id.toString()


        setCategorySpinner()
        setUnitSpinner()
        setCountrySpinner()
        setGradeSpinner()

        binding.spinnerCategory.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                selectedCategory = Constants.getCategoryIdFromAll(item, allCategories)


                CoroutineScope(Dispatchers.IO).launch {
                    spinnerSubCategorylist.clear()
                    callSubCategoryListApi()
                }

            })

        binding.tvAdd.setOnClickListener {
            productName = binding.etProductName.text.toString()
            description = binding.etProductDesc.text.toString()
            companyName = binding.etCompanyName.text.toString()

            price = binding.etPrice.text.toString()
            selectedCountry = ""

            if (productName.isNullOrEmpty()) {
                binding.etProductName.requestFocus()
                binding.etProductName.error = "Enter Product Name"
            }
            else if (description.isNullOrEmpty()) {
                binding.etProductDesc.requestFocus()
                binding.etProductDesc.error = "Enter Product Description"
            }else if (price.isNullOrEmpty()) {
                binding.etPrice.requestFocus()
                binding.etPrice.error = "Enter Product Price"
            } else if (selectedUnit == null) {
                binding.spinnerQuatity.requestFocus()
                binding.spinnerQuatity.error = "Select Unit"
            }else if (selectedCategory == null) {
                Toast.makeText(bContext, "Select Category", Toast.LENGTH_SHORT).show()
            } else if (selectedSubCategory == null) {
                Toast.makeText(bContext, "Select Subcategory", Toast.LENGTH_SHORT).show()
            } else if (selectedGrade == null) {
                Toast.makeText(bContext, "Select Grade", Toast.LENGTH_SHORT).show()
            } else if (imageAdapter.selectedImages.size == 0) {
                Toast.makeText(bContext, "Upload atleast one product image", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Constants.showProgressDialog(bContext, "Updating Product", "Please Wait...")
                CoroutineScope(Dispatchers.IO).launch {
                    callUpdateProductApi(
                        productName.toString(),
                        description.toString(),
                        companyName.toString(),
                        price.toString(),
                        selectedGrade.toString(),
                        selectedUnit!!.toString(),
                        selectedCategory!!.toString(),
                        selectedSubCategory!!.toString(),
                        selectedCountry.toString()
                    )
                }
            }


        }


    }

    private fun callUpdateProductApi(
        productName: String,
        description: String,
        companyName:String,
        price: String,
        selectedGrade: String,
        selectedUnit: String,
        selectedCategory: String,
        selectedSubCategory: String,
        selectedCountry: String
    ) {

        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {

                val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .addNetworkInterceptor(StethoInterceptor())
                    .readTimeout(300, TimeUnit.SECONDS)
                    .build()

                try {
                    if (imageAdapter.selectedImages.size != 0) {

                        for (i in imageAdapter.selectedImages) {
                            imageNames.add((i))
                        }

                        val multiPartBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)

                        for (i in imageNames.indices) {
                            val file = File(
                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                                    .toString()
                                        + bContext!!.getString(R.string.compressed_image_location) + "/" + imageNames[i].toString()
                            )
                            if (file.exists()) {
                                val MEDIA_TYPE = MediaType.parse("image/jpg")
                                multiPartBuilder.addFormDataPart(
                                    "product_images[]",
                                    imageNames[i],
                                    RequestBody.create(MEDIA_TYPE, file)
                                )
                            } else Log.d("file", "sendDataToServer: File Not found")
                        }
                        multiPartBuilder.addFormDataPart("product_id", product.id.toString())
                        multiPartBuilder.addFormDataPart("product_name", productName)
                        multiPartBuilder.addFormDataPart("product_description", description)
                        multiPartBuilder.addFormDataPart("product_price", price)
                        multiPartBuilder.addFormDataPart("product_unit", selectedUnit)
                        multiPartBuilder.addFormDataPart("category_id", selectedCategory)
                        multiPartBuilder.addFormDataPart("subcategory_id", selectedSubCategory)
                        multiPartBuilder.addFormDataPart("country_name", selectedCountry)
                        multiPartBuilder.addFormDataPart("grade_id", selectedGrade)
                        multiPartBuilder.addFormDataPart("company_name", companyName)


                        val requestBody: RequestBody = multiPartBuilder.build()


                        val request: Request = Request.Builder()
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json")
                            .addHeader(
                                "Authorization",
                                "Bearer ${PowerPreference.getDefaultFile().getString("token")}"
                            )
                            .url(ApiClient.BASE_URL + "productUpdate")
                            .post(requestBody)
                            .build()
                        try {
                            val response: Response = okHttpClient.newCall(request).execute()
                            if (response.isSuccessful()) {
                                serResponse = response.body()!!.string()
                                imageNames.clear()
                                sendMsgToMainThread(serResponse)
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                            imageNames.clear()
                            sendMsgToMainThread(e.message)

                        }
                    }else
                    {
                        sendMsgToMainThread("Add Atleast One Image of Product")

                    }


                } catch (e: Exception) {

                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private fun setUnitSpinner() {
        binding.spinnerQuatity.setItems("1 Piece", "2 Pieces", "3 Piece", "4 Piece", "5 Piece",
            "6 Piece","7 Piece", "8 Piece", "9 Piece", "10 Piece")
        binding.spinnerQuatity.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                selectedUnit = item
            })
    }

    private fun setCountrySpinner() {
        for (locale in Locale.getAvailableLocales()) {
            if (!TextUtils.isEmpty(locale.displayCountry)) {
                spinnerCountrylist.add(locale.displayCountry)
            }
        }

        val adapter: ArrayAdapter<String> = ArrayAdapter(
            bContext as UpdateProductActivity,
            R.layout.support_simple_spinner_dropdown_item,
            spinnerCountrylist
        )

        binding.spinnerCountry.threshold = 3
        binding.spinnerCountry.setAdapter(adapter)
    }


    private fun setGradeSpinner() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllGradeList(
                            Constants.getHeadersWithToken()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: GetGradeResponse? = response.body()
                            if (data!!.status == Constants.SUCCESS) {

                                allGradeList = data.data.product_grades

                                for (i in data.data.product_grades.indices) {
                                    spinnerGradelist.add(data.data.product_grades[i].name)
                                }

                                val adapter: ArrayAdapter<String> = ArrayAdapter(
                                    bContext as UpdateProductActivity,
                                    R.layout.support_simple_spinner_dropdown_item,
                                    spinnerGradelist
                                )

                                binding.spinnerGrade.setAdapter(adapter)
                                binding.spinnerGrade.selectedIndex =
                                    Constants.getSelectedIndexforGrade(product.grade_id,
                                        allGradeList  as MutableList<ProductGrade>
                                    )

                            } else {
                                sendMsgToMainThread(data.status)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }


    }


    private fun setCategorySpinner() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllCategories(Constants.getHeadersWithToken())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: CategoryResponse? = response.body()
                            sendDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }


    private suspend fun sendDataToMainThread(data: CategoryResponse?) {

        withContext(Dispatchers.Main)
        {
            if (data!!.categories.isEmpty()) {
                sendMsgToMainThread("You have to add atleast one category first")
            } else {
                allCategories = data.categories

                for (i in data.categories.indices) {
                    spinnerCategorylist.add(data.categories[i].name)
                }

                val adapter: ArrayAdapter<String> = ArrayAdapter(
                    bContext as UpdateProductActivity,
                    R.layout.support_simple_spinner_dropdown_item,
                    spinnerCategorylist
                )

                binding.spinnerCategory.setAdapter(adapter)
                binding.spinnerCategory.selectedIndex =
                    Constants.getSelectedIndex(product.category_id,
                        allCategories as MutableList<com.kspl.smitglobal.modelClasses.CategoryResponse.Category>
                    )

            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main) {
            Constants.dismiss()


            if (message != null) {
                if (message.contains("success")) {
                    toast("Product Updated Successfully")
                    closeActivity()
                } else {
                    toast(message.toString())
                }
            }

        }
    }

    private fun callSubCategoryListApi() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllSubCategory(
                            Constants.getHeadersWithToken(),
                            selectedCategory.toString()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: SubCategoryResponse? = response.body()
                            if (data!!.status == "success") {
                                sendDataToMainThreadForSubCat(data)
                            } else {
                                sendMsgToMainThread(data.status)
                            }

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private suspend fun sendDataToMainThreadForSubCat(data: SubCategoryResponse) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.data.sub_categories.isEmpty()) {
                sendMsgToMainThread("You Have to add Sub-Category for selected category first")
                binding.spinnerSubCategory.hint = "No Sub-Category Found"

            } else {

                allSubCategories = data.data.sub_categories
                for (i in data.data.sub_categories.indices) {
                    spinnerSubCategorylist.add(data.data.sub_categories[i].name)
                }

                val adapter: ArrayAdapter<String> = ArrayAdapter(
                    bContext as UpdateProductActivity,
                    R.layout.support_simple_spinner_dropdown_item,
                    spinnerSubCategorylist
                )
                binding.spinnerSubCategory.setAdapter(adapter)
                binding.spinnerSubCategory.selectedIndex =
                    Constants.getSelectedIndexforSubcat(product.sub_category_id,
                        allSubCategories as MutableList<SubCategory>
                    )

            }
        }
    }

    fun checkAndRequestPermissions(context: Activity?): Boolean {

        val WriteExtstorePermission = ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val ReadExtstorePermission = ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val cameraPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.CAMERA
        )
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (WriteExtstorePermission !== PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (ReadExtstorePermission !== PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                context, listPermissionsNeeded.toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> if (ContextCompat.checkSelfPermission(
                    this@UpdateProductActivity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(
                    applicationContext,
                    "App Requires Access to Camara.", Toast.LENGTH_SHORT
                )
                    .show()
            } else if (ContextCompat.checkSelfPermission(
                    this@UpdateProductActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(
                    applicationContext,
                    "App Requires Access to Your Storage.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                showCameraGalaryDialog()
            }
        }
    }

    private fun showCameraGalaryDialog() {
        val options =
            arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder =
            AlertDialog.Builder(bContext)
        builder.setTitle("Choose picture options")

        builder.setItems(options) { dialogInterface, i ->
            if (options.get(i).equals("Take Photo")) {
                cameraTask()
            } else if (options.get(i).equals("Choose from Gallery")) {
                GalleryTask()
            } else {
                dialogInterface.dismiss()
            }
        }
        builder.show()
    }


    private fun cameraTask() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val dir = File(
            "" + getExternalFilesDir(Environment.DIRECTORY_PICTURES) + getString(R.string.actual_image_location)
        )
        if (!dir.exists()) {
            dir.mkdirs()
        }
        imgName = "SMIT-" + System.currentTimeMillis() + ".jpg"
        destination = File(dir, imgName)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination))
        startActivityForResult(intent, REQUEST_CAMERA)
    }

    private fun GalleryTask() {

        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, SELECT_FILE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data)
            } else if (requestCode == SELECT_FILE) {
                ongallerySelectResult(data)
            }
        }
    }

    private fun onCaptureImageResult(data: Intent?) {
        if (destination != null) {
            val bmOptions = BitmapFactory.Options()
            val photo = BitmapFactory.decodeFile(destination!!.absolutePath, bmOptions)

            val obj = CompressImage(bContext)
            obj.compressImage(obj.getImageUri(photo).toString(), destination!!.absolutePath)
            imgName = obj.compressedImageName

            val imageModel = ImageModel()
            imageModel.imageLocation = obj.compressedImageLocation
            imageModel.imageName = obj.compressedImageName
            addImages(imageModel)


        } else {
            Toast.makeText(bContext, "Not Enough Space", Toast.LENGTH_SHORT).show()
        }
    }

    private fun ongallerySelectResult(data: Intent?) {

        val selectedImage = data!!.data
        val filePathColumn =
            arrayOf(MediaStore.Images.Media.DATA)
        // Get the cursor
        // Get the cursor
        val cursor: Cursor? = contentResolver.query(
            selectedImage!!,
            filePathColumn, null, null, null
        )
        // Move to first row
        // Move to first row
        cursor!!.moveToFirst()
        val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
        val imgDecodableString: String = cursor.getString(columnIndex)
        cursor.close()
        val photo: Bitmap = BitmapFactory.decodeFile(imgDecodableString)

        val obj = CompressImage(bContext)
        obj.compressImage(obj.getImageUri(photo).toString(), "")
        imgName = obj.compressedImageName
        val imageModel = ImageModel()
        imageModel.imageLocation = obj.compressedImageLocation
        imageModel.imageName = obj.compressedImageName
        addImages(imageModel)

    }

    private fun addImages(imageModel: ImageModel) {
        imageAdapter.addItem(imageModel)
        binding.rvImages.visibility = View.VISIBLE
        layoutManager = LinearLayoutManager(this)
        layoutManager!!.setOrientation(RecyclerView.HORIZONTAL)
        binding.rvImages.setAdapter(imageAdapter)
        binding.rvImages.setLayoutManager(layoutManager)
        binding.rvImages.setHasFixedSize(true)
    }


    private suspend fun sendMsg(msg: String?) {
        withContext(Dispatchers.Main) {
            dissmissDialog()
            if (msg != null) {
                toast(msg)
            }
        }
    }

    override fun onDeletePressed(imageModel: ImageModel?, postion: Int) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to delete this image")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                if(imageModel!!.imageName.contains("product_images"))
                {
                    deleteImage(imageModel,postion)
                }else
                {
                    imageAdapter.removeItem(imageModel, postion!!)
                }
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()

    }
    private fun deleteImage(imageModel: ImageModel?,position:Int?) {
        Constants.showProgressDialog(
            bContext,
            "Deleting Image", "Please wait"
        )
        CoroutineScope(Dispatchers.IO).launch {

            val string: String = imageModel!!.imageName
            val imageList: List<String> = string.split("/")
            val imageName = imageList[1]
            try {
                val response = ApiClient.apiService.deleteImage(
                    headerWithToken(),
                    product.id.toString(), imageName
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "success") {
                            deleteImageMsg("Deleted Successfully")
                            imageAdapter.removeItem(imageModel,position!!)
                        } else {
                            deleteImageMsg(response.body()!!.message)
                        }
                    } else {
                        deleteImageMsg("Something went wrong")
                    }
                } else {
                    deleteImageMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                deleteImageMsg(e.message)
            }
        }

    }

    private suspend fun deleteImageMsg(msg: String?) {
        withContext(Dispatchers.Main) {
            dissmissDialog()
            Toast.makeText(bContext, msg, Toast.LENGTH_SHORT).show()

        }
    }

    private fun closeActivity() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}