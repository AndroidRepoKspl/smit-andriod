package com.kspl.smitglobal.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.ActivitySplashBinding
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SplashActivity : BaseActivity() {

    lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_splash)



        val animation =
            AnimationUtils.loadAnimation(this, R.anim.fade_in)

        binding.imageView.animation = animation

        val i = Intent(this, LoginActivity::class.java)
        val timer: Thread = object : Thread() {
            override fun run() {
                try {
                    sleep(2500)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    if (PowerPreference.getDefaultFile().getBoolean("isLogin")) {
                        CoroutineScope(Dispatchers.Main).launch {
                            sendToActivity()
                        }
                    }else
                    {
                        startActivity(i)
                        finish()
                    }

                }
            }
        }
        timer.start()
    }

    private suspend fun sendToActivity() {
        withContext(Dispatchers.Main){
            if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
                val intent = Intent(bContext, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(bContext, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
    }
}