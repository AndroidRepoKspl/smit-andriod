





















































































package com.kspl.smitglobal.activities


import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.jaredrummler.materialspinner.MaterialSpinner
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.api.ApiClient.Companion.apiService
import com.kspl.smitglobal.databinding.*
import com.kspl.smitglobal.modelClasses.AddCategoryResponse.AddCategoryResponse
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.modelClasses.GradeResponse.GetGradeResponse
import com.kspl.smitglobal.modelClasses.GradeResponse.ProductGrade
import com.kspl.smitglobal.modelClasses.UpdateUserDetils.UpdateUserResponse
import com.kspl.smitglobal.modelClasses.deleteGrade.DeleteGradeResponse
import com.kspl.smitglobal.utils.Constants
import com.kspl.smitglobal.utils.MoreMenuFactory
import com.preference.PowerPreference
import com.skydoves.powermenu.OnMenuItemClickListener
import com.skydoves.powermenu.kotlin.powerMenu
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategryActivity : BaseActivity() {

    lateinit var binding: ActivityCategryBinding
    lateinit var context: Context
    private val moreMenu by powerMenu(MoreMenuFactory::class)


    lateinit var addCategoryDialog: AlertDialog
    lateinit var addGradeDialog: AlertDialog
    lateinit var addSubCategoryDialog: AlertDialog
    lateinit var updateProfileDialog: AlertDialog
    lateinit var deleteGradeDialog: AlertDialog


    var categoryList: MutableList<Category> = ArrayList()
    var categorylistforspinner: MutableList<String> = ArrayList()

    private var selectedCategory: String? = null
    var selectedGrade: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_categry)
        context = this
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        binding.ivMenu.setOnClickListener {
            finish()
        }

        binding.cvAddCategory.setOnClickListener {
            openAdddCategoryDialog()
        }

        binding.cvAddSubCategory.setOnClickListener {
            reloadCategories()
        }

        binding.cvAddProduct.setOnClickListener {
            openAddProductActivity()
        }

        binding.cvAddGrade.setOnClickListener {
            openAddGradeDialog()
        }


        binding.cvAddBrochures.setOnClickListener {
            startActivity(Intent(bContext, ViewFilesActivity::class.java))
        }

        binding.cvDeleteGrade.setOnClickListener {
            openDeleteGradeDialog()
        }
    }


    fun reloadCategories() {
        Constants.showProgressDialog(context, "Loading All Category", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            callCategoryListApi()
        }
    }

    private fun openAddProductActivity() {
        val intent = Intent(this, AddProductActivity::class.java)
        startActivityForResult(intent, 111)
    }


    private fun openDeleteGradeDialog() {

        val dialogBinding: DeleteGradeDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.delete_grade_dialog, null, false
        )
        deleteGradeDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()


        var allGradeList: List<ProductGrade> = ArrayList()
        var spinnerGradelist: MutableList<String> = ArrayList()


        Constants.showProgressDialog(context, "Loading All Grades", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllGradeList(
                            Constants.getHeadersWithToken()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: GetGradeResponse? = response.body()
                            if (data!!.status == Constants.SUCCESS) {
                                withContext(Main) {
                                    dissmissDialog()

                                    allGradeList = data.data.product_grades

                                    for (i in data.data.product_grades.indices) {
                                        if(data.data.product_grades[i].id !=7){
                                            spinnerGradelist.add(data.data.product_grades[i].name)
                                        }
                                    }

                                    val adapter: ArrayAdapter<String> = ArrayAdapter(
                                        context as CategryActivity,
                                        R.layout.support_simple_spinner_dropdown_item,
                                        spinnerGradelist
                                    )

                                    dialogBinding.spinnerCategory.setAdapter(adapter)
                                }


                            } else {
                                sendMsgToMainThread(data.status)
                            }
                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }






        dialogBinding.tvAddSubCategoryCancel.setOnClickListener {
            spinnerGradelist.clear()
            deleteGradeDialog.dismiss()
        }



        dialogBinding.spinnerCategory.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                if (allGradeList.isNotEmpty()) {
                    selectedGrade = Constants.getGradeIdFromAll(item, allGradeList)
                }
            })


        dialogBinding.tvAddSubCategory.setOnClickListener {

            if (selectedGrade.isNullOrBlank()) {
                Toast.makeText(context, "Please Select Grade", Toast.LENGTH_SHORT).show()
            } else {
                Constants.showProgressDialog(
                    context,
                    "Deleting Grade",
                    "Please Wait"
                )
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = apiService.deleteGrade(
                            Constants.getHeadersWithToken(), selectedGrade!!.toInt()
                        )
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                val data: DeleteGradeResponse? = response.body()
                                sendMsg(data!!.message)
                            } else {
                                sendMsg("Something went wrong")
                            }
                        } else {
                            sendMsg("Something went wrong")
                        }
                    } catch (e: java.lang.Exception) {
                        sendMsg(e.message!!)
                    }
                }
            }

        }


    }

    private fun openAddGradeDialog() {
        val dialogBinding: DialogAddGradeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_add_grade, null, false
        )
        addCategoryDialog = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.tvAddCategoryCancel.setOnClickListener { addCategoryDialog.dismiss() }

        dialogBinding.tvAddCstegory.setOnClickListener {

            if (dialogBinding.etCategoryName.text!!.isEmpty()) {
                dialogBinding.etCategoryName.requestFocus()
                dialogBinding.etCategoryName.error = "Enter Grade Name"
            } else {
                Constants.showProgressDialog(
                    context,
                    "Adding Grade",
                    "Please Wait"
                )
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = apiService.addGrade(
                            Constants.getHeadersWithToken(),
                            dialogBinding.etCategoryName.text.toString()
                        )
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                val data: AddCategoryResponse? = response.body()
                                sendMsg(data!!.message)
                            } else {
                                sendMsg("Something went wrong")
                            }
                        } else {
                            sendMsg("Something went wrong")
                        }
                    } catch (e: java.lang.Exception) {
                        sendMsg(e.message!!)
                    }
                }
            }
        }


    }


    private suspend fun callCategoryListApi() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllCategories(Constants.getHeadersWithToken())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: CategoryResponse? = response.body()
                            sendDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private suspend fun sendDataToMainThread(data: CategoryResponse?) {

        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.categories.isEmpty()) {
                Toast.makeText(bContext, "No Category Found Add Category First", Toast.LENGTH_LONG)
                    .show()
            } else {
                categoryList = data.categories as MutableList<Category>
                openAdddSubCategoryDialog()

            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(context, it, Toast.LENGTH_LONG).show() }
            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }


    private fun openUpdateProfileDialog() {
        val dialogBinding: DialogUpdateProfileBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.dialog_update_profile, null, false
            )
        updateProfileDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.etName.setText(PowerPreference.getDefaultFile().getString("name"))
        dialogBinding.etEmail.setText(PowerPreference.getDefaultFile().getString("email"))
        dialogBinding.etPhoneNum.setText(PowerPreference.getDefaultFile().getString("contact_no"))


        dialogBinding.cancel.setOnClickListener {
            updateProfileDialog.dismiss()
        }

        dialogBinding.update.setOnClickListener {

            var userid = PowerPreference.getDefaultFile().getString("userid")
            var name = dialogBinding.etName.text.toString()
            var email = dialogBinding.etEmail.text.toString()

            if (name.isNullOrBlank()) {
                dialogBinding.etName.requestFocus()
                dialogBinding.etName.error = "Name cannot be empty"
            } else if (email.isNullOrBlank()) {
                dialogBinding.etEmail.requestFocus()
                dialogBinding.etEmail.error = "Email cannot be empty"
            } else {
                Constants.showProgressDialog(context, "Updating Profile", "Please Wait")

                CoroutineScope(Dispatchers.IO).launch {
                    if (Constants.isNetworkAvailable()) {
                        try {
                            val response =
                                ApiClient.apiService.updateProfile(
                                    Constants.getHeadersWithToken(),
                                    userid,
                                    name,
                                    "",
                                    email
                                )
                            if (response.isSuccessful) {
                                if (response.body() != null) {
                                    val data: UpdateUserResponse? = response.body()
                                    sendMsg(data!!.message)
                                    PowerPreference.getDefaultFile().apply {
                                        setString("name", name)
                                        setString("email", email)


                                    }

                                } else {
                                    sendMsg(Constants.NULL_RESPONSE)
                                }
                            } else {
                                sendMsg(response.message())
                            }

                        } catch (e: Exception) {
                            sendMsg(e.message!!)
                        }
                    } else {
                        sendMsg(Constants.NO_INTERNET)
                    }
                }
            }


        }
    }


    private fun openAdddCategoryDialog() {
        val dialogBinding: DialogAddCategoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_add_category, null, false
        )
        addGradeDialog = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.tvAddCategoryCancel.setOnClickListener { addGradeDialog.dismiss() }

        dialogBinding.tvAddCstegory.setOnClickListener {

            if (dialogBinding.etCategoryName.text!!.isEmpty()) {
                dialogBinding.etCategoryName.requestFocus()
                dialogBinding.etCategoryName.error = "Enter Category Name"
            } else {
                Constants.showProgressDialog(
                    context,
                    "Adding Category",
                    "Please Wait"
                )
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = apiService.addCategory(
                            Constants.getHeadersWithToken(),
                            dialogBinding.etCategoryName.text.toString()
                        )
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                val data: AddCategoryResponse? = response.body()
                                sendMsg(data!!.message)
                            } else {
                                sendMsg("Something went wrong")
                            }
                        } else {
                            sendMsg("Something went wrong")
                        }
                    } catch (e: java.lang.Exception) {
                        sendMsg(e.message!!)
                    }
                }
            }
        }


    }

    private suspend fun sendMsg(msg: String) {
        withContext(Main) {
            Constants.dismiss()
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

            if (::addCategoryDialog.isInitialized) {
                addCategoryDialog.dismiss()
            }
            if (::addGradeDialog.isInitialized) {
                addGradeDialog.dismiss()
            }
            if (::addSubCategoryDialog.isInitialized) {
                categorylistforspinner.clear()
                addSubCategoryDialog.dismiss()
            }
            if (::addGradeDialog.isInitialized) {
                addGradeDialog.dismiss()
            }
            if (::deleteGradeDialog.isInitialized) {
                deleteGradeDialog.dismiss()
            }
            if (::updateProfileDialog.isInitialized) {
                updateProfileDialog.dismiss()
            }

        }
    }

    private fun openAdddSubCategoryDialog() {
        val dialogBinding: DialogAddSubCategoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_add_sub_category, null, false
        )
        addSubCategoryDialog = android.app.AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.tvAddSubCategoryCancel.setOnClickListener {
            categorylistforspinner.clear()
            addSubCategoryDialog.dismiss()
        }

        for (i in categoryList.indices) {
            categorylistforspinner.add(categoryList[i].name)
        }

        val adapter: ArrayAdapter<String> = ArrayAdapter(
            context,
            R.layout.support_simple_spinner_dropdown_item,
            categorylistforspinner
        )

        dialogBinding.spinnerCategory.setAdapter(adapter)

        dialogBinding.spinnerCategory.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<String> { view, position, id, item ->
                selectedCategory = Constants.getCategoryIdFromAll(item, categoryList)
            })


        dialogBinding.tvAddSubCategory.setOnClickListener {

            if (dialogBinding.etSubCategoryName.text!!.isEmpty()) {
                dialogBinding.etSubCategoryName.requestFocus()
                dialogBinding.etSubCategoryName.error = "Enter Sub Category Name"
            } else if (selectedCategory.isNullOrBlank()) {
                Toast.makeText(context, "Please Select Category", Toast.LENGTH_SHORT).show()
            } else {
                Constants.showProgressDialog(
                    context,
                    "Adding Sub-Category",
                    "Please Wait"
                )
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val response = apiService.addSubCategory(
                            Constants.getHeadersWithToken(),
                            dialogBinding.etSubCategoryName.text.toString(),
                            selectedCategory.toString()
                        )
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                val data: AddCategoryResponse? = response.body()
                                sendMsg(data!!.message)
                            } else {
                                sendMsg("Something went wrong")
                            }
                        } else {
                            sendMsg("Something went wrong")
                        }
                    } catch (e: java.lang.Exception) {
                        sendMsg(e.message!!)
                    }
                }
            }

        }


    }

}