package com.kspl.smitglobal.activities

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.FilesAdapter
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityViewFilesBinding
import com.kspl.smitglobal.databinding.AddFileFolderLayoutBinding
import com.kspl.smitglobal.databinding.FileMenuDialogLayoutBinding
import com.kspl.smitglobal.modelClasses.AddCategoryResponse.AddCategoryResponse
import com.kspl.smitglobal.modelClasses.fileSystem.getfiles.Data
import com.kspl.smitglobal.modelClasses.fileSystem.getfiles.GetFiles
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ViewFilesActivity : BaseActivity(), FilesAdapter.FileCliked {
    private var parentId = ""
    private var root = ""
    private var root_name = ""
    private lateinit var binding: ActivityViewFilesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_files)

        if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
            binding.floatingActionButton.hide()
        }else
        {
            binding.floatingActionButton.show()
        }

        getRoot()

        binding.ivBack.setOnClickListener { onBackApiCall() }

        binding.floatingActionButton.setOnClickListener {
            showFileDialog()
        }
    }

    private fun getRoot() {
        Constants.showProgressDialog(
            bContext,
            "Loading Files",
            "Please Wait"
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.getRootDirectory(
                    Constants.getHeadersWithToken()
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val data: GetFiles? = response.body()
                        if(data?.status==Constants.SUCCESS){

                            parentId = "0"
                            root_name = "root:"

                            val list = data.data
                            setDataToRv(list,root_name)
                        }

                    } else {
                        sendMsg("Something went wrong")
                    }
                } else {
                    sendMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message!!)
            }
        }
    }

    private fun showFileDialog() {
        val dialogBinding: FileMenuDialogLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(bContext),
            R.layout.file_menu_dialog_layout, null, false
        )
        val fileDialog: AlertDialog = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.tvMenuCancel.setOnClickListener {
            fileDialog.dismiss()
        }

        dialogBinding.tvAddFile.setOnClickListener {
            fileDialog.dismiss()
            openAddFileDialog()
        }

        dialogBinding.tvAddFolder.setOnClickListener {
            fileDialog.dismiss()
            openAddFolderDialog()
        }
    }

    private fun openAddFileDialog() {
        val dialogBinding: AddFileFolderLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(bContext),
            R.layout.add_file_folder_layout, null, false
        )
        val fileDialog: AlertDialog = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.textView6.text = "Add File"
        dialogBinding.etFileFolderName.hint = "File Name"
        dialogBinding.editTextLink.visibility = View.VISIBLE

        dialogBinding.tvCancel.setOnClickListener {
            fileDialog.dismiss()
        }

        dialogBinding.tvAddButton.setOnClickListener {
            val folderName= dialogBinding.etFileFolderName.text.toString().trim()
            val link= dialogBinding.etFileLink.text.toString().trim()
            when {
                folderName.isEmpty() -> {
                    dialogBinding.etFileFolderName.error = "Folder Name is Empty"
                    dialogBinding.etFileFolderName.requestFocus()
                }
                link.isEmpty() -> {
                    dialogBinding.etFileLink.error = "Link is Empty"
                    dialogBinding.etFileLink.requestFocus()
                }
                else -> {
                    callAddApi(folderName, parentId, link, "1", fileDialog)
                }
            }
        }
    }

    private fun openAddFolderDialog() {
        val dialogBinding: AddFileFolderLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(bContext),
            R.layout.add_file_folder_layout, null, false
        )
        val fileDialog: AlertDialog = AlertDialog.Builder(this)
            .setCancelable(false)
            .setView(dialogBinding.root)
            .show()

        dialogBinding.tvCancel.setOnClickListener {
            fileDialog.dismiss()
        }

        dialogBinding.tvAddButton.setOnClickListener {
            val folderName= dialogBinding.etFileFolderName.text.toString().trim()
            if(folderName.isEmpty()){
                dialogBinding.etFileFolderName.error = "Folder Name is Empty"
                dialogBinding.etFileFolderName.requestFocus()
            }else
            {
                callAddApi(folderName, parentId, null, "0", fileDialog)
            }
        }
    }

    private fun callAddApi(
        name: String,
        parentPage: String,
        link: String?,
        status: String,
        dialog: AlertDialog
    ) {
        Constants.showProgressDialog(
            bContext,
            "Adding",
            "Please Wait"
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.addFileFolder(
                    Constants.getHeadersWithToken(), name, parentPage, link, status
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val data: AddCategoryResponse? = response.body()
                        if(data?.status==Constants.SUCCESS){
                            clearDialog(dialog, data.message)
                        }else
                        {
                            data?.message?.let { sendMsg(it) }
                        }
                    } else {
                        sendMsg("Something went wrong")
                    }
                } else {
                    sendMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message!!)
            }
        }
    }

    private suspend fun clearDialog(dialog: AlertDialog, message: String) {
        withContext(Main) {
            Constants.dismiss()
            dialog.dismiss()
            Toast.makeText(bContext, message, Toast.LENGTH_SHORT).show()
            refresh(parentId)

        }
    }

    private fun refresh(num: String) {
        val rootId: String = if(root ==""){
            "0"
        }else{
            root
        }
        val rootName: String = if(root_name==""){
            "root:"
        }else{
            root_name
        }
        Constants.showProgressDialog(
            bContext,
            "Loading Files",
            "Please Wait"
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.getFiles(
                    Constants.getHeadersWithToken(),
                    num,rootId,
                    rootName
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val data: GetFiles? = response.body()
                        if(data?.status==Constants.SUCCESS){
                            this@ViewFilesActivity.parentId = data.parent_id
                            if(parentId==""){
                                parentId = "0";
                            }
                            this@ViewFilesActivity.root_name = data.root_name
                            if(root_name==""){
                                root_name = "root:"
                            }
                            this@ViewFilesActivity.root = data.root

                            val list = data.data
                            setDataToRv(list, rootName)
                        }

                    } else {
                        sendMsg("Something went wrong")
                    }
                } else {
                    sendMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message!!)
            }
        }
    }

    private fun getFiles(num: String, fileName: String) {


        val rootId: String = if(root ==""){
            "0"
        }else{
            "$root,$parentId"
        }

        val rootName: String = if(root_name==""){
            "root:"
        }else{
            "$root_name,$fileName"
        }

        Constants.showProgressDialog(
            bContext,
            "Loading Files",
            "Please Wait"
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.getFiles(
                    Constants.getHeadersWithToken(),
                    num,rootId,rootName
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val data: GetFiles? = response.body()
                        if(data?.status==Constants.SUCCESS){
                            this@ViewFilesActivity.parentId = data.parent_id
                            if(parentId==""){
                                parentId = "0";
                            }
                            this@ViewFilesActivity.root_name = data.root_name
                            if(root_name==""){
                                root_name = "root:"
                            }
                            this@ViewFilesActivity.root = data.root

                            val list = data.data
                            setDataToRv(list, root_name)
                        }

                    } else {
                        sendMsg("Something went wrong")
                    }
                } else {
                    sendMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message!!)
            }
        }
    }

    private suspend fun setDataToRv(
        list: List<Data>,
        rootName: String
    ) {
        withContext(Main){
            dissmissDialog()
            val dirname = rootName.replace(",","/")
            binding.tvPath.text = dirname
            if(list.isNotEmpty()){
                binding.rvFiles.visibility = View.VISIBLE
                binding.tvEmpty.visibility = View.GONE
                binding.rvFiles.layoutManager = GridLayoutManager(this@ViewFilesActivity, 3)
                binding.rvFiles.setHasFixedSize(true)
                binding.rvFiles.setItemViewCacheSize(list.size)
                binding.rvFiles.adapter = FilesAdapter(list, this@ViewFilesActivity)
            }else
            {
                binding.rvFiles.visibility = View.GONE
                binding.tvEmpty.visibility = View.VISIBLE
            }

        }

    }

    private suspend fun sendMsg(msg: String) {
        withContext(Main) {
            Constants.dismiss()
            Toast.makeText(bContext, msg, Toast.LENGTH_SHORT).show()

        }
    }

    override fun onFileClicked(file: Data) {
        if(file.status==0){

            getFiles(file.id.toString(),file.file_name)
        }else{
            openFileinBrowser(file.link)
        }
        
    }

    override fun onDeleteClicked(file: Data) {
        delete(file.id,file.file_name)
    }

    private fun delete(fileId: Int, fileName: String) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Delete $fileName?")
        builder.setMessage("Are you sure you want to Delete $fileName?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                callDeleteApi(fileId)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun callDeleteApi(id: Int) {
        Constants.showProgressDialog(
            bContext,
            "Deleting",
            "Please Wait"
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.deleteFile(
                    Constants.getHeadersWithToken(), id
                )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        val data: AddCategoryResponse? = response.body()
                        if(data?.status==Constants.SUCCESS){
                           refreshPage(data.message)
                        }else
                        {
                            data?.message?.let { sendMsg(it) }
                        }
                    } else {
                        sendMsg("Something went wrong")
                    }
                } else {
                    sendMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                sendMsg(e.message!!)
            }
        }
    }

    private suspend fun refreshPage(message: String) {
        withContext(Main){
            dissmissDialog()
            toast(message)
            refresh(parentId)
        }
    }

    private fun openFileinBrowser(url: String) {
        try {
            var webpage = Uri.parse(url)
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                webpage = Uri.parse("http://$url");
            }
            val intent = Intent(Intent.ACTION_VIEW, webpage)
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }catch (e: Exception){
            e.message?.let { toast(it) }
        }

    }

    override fun onBackPressed() {
        onBackApiCall()
    }

    private fun onBackApiCall() {


        if(root ==""){
            exitAlert()
        }else
        {
            Constants.showProgressDialog(
                bContext,
                "Loading Files",
                "Please Wait"
            )
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = ApiClient.apiService.onBackApi(
                        Constants.getHeadersWithToken(),
                        root,root_name
                    )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: GetFiles? = response.body()
                            if(data?.status==Constants.SUCCESS){

                                this@ViewFilesActivity.parentId = data.parent_id
                                if(parentId==""){
                                    parentId = "0";
                                }
                                this@ViewFilesActivity.root_name = data.root_name
                                if(root_name==""){
                                    root_name = "root:"
                                }
                                this@ViewFilesActivity.root = data.root

                                val list = data.data
                                setDataToRv(list, root_name)
                            }

                        } else {
                            sendMsg("Something went wrong")
                        }
                    } else {
                        sendMsg("Something went wrong")
                    }
                } catch (e: java.lang.Exception) {
                    sendMsg(e.message!!)
                }
        }


        }
    }

    private fun exitAlert() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Exit")
        builder.setMessage("Are you sure you want to Exit Brochures?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                finish()
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }
}