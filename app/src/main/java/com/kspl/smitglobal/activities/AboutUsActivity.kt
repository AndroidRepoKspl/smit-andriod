package com.kspl.smitglobal.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.ActivityAboutUsBinding

class AboutUsActivity : BaseActivity() {
    lateinit var binding :ActivityAboutUsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_about_us)
    }
}