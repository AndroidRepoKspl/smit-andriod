package com.kspl.smitglobal.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.EnqiryDetailsAdaptor
import com.kspl.smitglobal.adaptors.EnquiryAdapter
import com.kspl.smitglobal.adaptors.EnquiryProductAdaptor
import com.kspl.smitglobal.adaptors.UsersAdaptor
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityUserEnquiryListBinding
import com.kspl.smitglobal.databinding.ActivityUsersBinding
import com.kspl.smitglobal.modelClasses.EnquiryProductsResponse.EnquiryProductsResponse
import com.kspl.smitglobal.modelClasses.UserEnquiryResponse.UserEnquiryResponse
import com.kspl.smitglobal.modelClasses.UsersResponse.Data
import com.kspl.smitglobal.modelClasses.UsersResponse.UsersResponse
import com.kspl.smitglobal.modelClasses.products.Product
import com.kspl.smitglobal.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserEnquiryListActivity : BaseActivity(), EnqiryDetailsAdaptor.EnquiryClick {

    lateinit var binding: ActivityUserEnquiryListBinding
    lateinit var users: Data
    lateinit var cameFrom: String
    lateinit var id: String

    var enquiryList: MutableList<com.kspl.smitglobal.modelClasses.UserEnquiryResponse.Data>? =
        ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_enquiry_list)


        cameFrom = intent.getStringExtra("cameFrom").toString()


        if (cameFrom != "null") {
            users = intent.getSerializableExtra("user") as Data
            binding.ivCall.setOnClickListener {
                val builder = AlertDialog.Builder(bContext)
                builder.setMessage("Are you sure you want to call ${users.name} ?")
                    .setCancelable(false)
                    .setPositiveButton("Yes") { dialog, id ->
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:${users.mobile_no}")
                        bContext.startActivity(intent)
                    }
                    .setNegativeButton("No") { dialog, id -> dialog.cancel() }
                val alert = builder.create()
                alert.show()
            }
            binding.tvUserName.text = users.name
            callUsersEnquiryApi(users.id.toString())
        } else {
            id = intent.getStringExtra("id").toString()
            binding.tvUserName.text = "Enquiry History"
            binding.ivCall.visibility = View.GONE
            callUsersEnquiryApi(id)

        }

        binding.ivback.setOnClickListener { finish() }

    }

    private fun callUsersEnquiryApi(id: String) {
        Constants.showProgressDialog(bContext, "Loading All Enquiries", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getUserEnquiryList(Constants.getHeadersWithToken(), id)
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: UserEnquiryResponse? = response.body()
                            sendDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }


    private suspend fun sendDataToMainThread(data: UserEnquiryResponse?) {

        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.data.isEmpty()) {
                sendMsgToMainThread("No Data Found")
                binding.rvEnquiryNo.visibility = View.GONE
            } else {

                for (i in data.data.indices) {
                    if (i == 0) {
                        val mdata = com.kspl.smitglobal.modelClasses.UserEnquiryResponse.Data(
                            true,
                            data.data[i].created_at,
                            data.data[i].created_by,
                            data.data[i].enquiry_status,
                            data.data[i].id,
                            data.data[i].updated_at
                        )
                        enquiryList!!.add(mdata)
                        setItemData(data.data.get(i).id)
                    } else {
                        enquiryList!!.add(data.data[i])
                    }
                }



                binding.rvEnquiryNo.visibility = View.VISIBLE
                binding.rvEnquiryNo.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    setHasFixedSize(true)
                    setItemViewCacheSize(enquiryList!!.size)
                    adapter = EnqiryDetailsAdaptor(enquiryList, this@UserEnquiryListActivity)
                }
            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(bContext, it, Toast.LENGTH_LONG).show() }
            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onEnquiryClick(
        data: com.kspl.smitglobal.modelClasses.UserEnquiryResponse.Data,
        position: Int
    ) {
        setItemData(data.id)

    }

    private fun setItemData(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getEnquiryProdctList(
                            Constants.getHeadersWithToken(),
                            id.toString()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: EnquiryProductsResponse? = response.body()
                            sendProductDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }

    private suspend fun sendProductDataToMainThread(data: EnquiryProductsResponse?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.data.isEmpty()) {
                sendMsgToMainThread("No Data Found")
                binding.rvEnquiryProducts.visibility = View.GONE
            } else {
                binding.rvEnquiryProducts.visibility = View.VISIBLE
                binding.rvEnquiryProducts.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    setHasFixedSize(true)
                    setItemViewCacheSize(data.data.size)
                    adapter = EnquiryProductAdaptor(data.data)
                }
            }
        }
    }


}