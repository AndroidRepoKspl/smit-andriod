package com.kspl.smitglobal.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.CategoryAdaptor
import com.kspl.smitglobal.adaptors.UsersAdaptor
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityUsersBinding
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.modelClasses.UsersResponse.Data
import com.kspl.smitglobal.modelClasses.UsersResponse.UsersResponse
import com.kspl.smitglobal.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.Serializable

class UsersActivity : BaseActivity(), UsersAdaptor.ItemClick {

    lateinit var binding:ActivityUsersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_users)


        callUsersApi()

        binding.ivback.setOnClickListener {
            finish()
        }


    }

    private fun callUsersApi() {
        Constants.showProgressDialog(bContext, "Loading All Users", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllUserList(Constants.getHeadersWithToken())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: UsersResponse? = response.body()
                            sendDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }
    }

    private suspend fun sendDataToMainThread(data: UsersResponse?) {

        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.data.isEmpty()) {
                sendMsgToMainThread("No Data Found")
                binding.rvUsers.visibility = View.GONE
                binding.tvNoDataFound.visibility = View.VISIBLE
            } else {
                binding.rvUsers.visibility = View.VISIBLE
                binding.tvNoDataFound.visibility = View.GONE
                binding.rvUsers.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    setItemViewCacheSize(data.data.size)
                    adapter = UsersAdaptor(data.data,this@UsersActivity)
                }
            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(bContext, it, Toast.LENGTH_LONG).show() }
            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onItemClick(data: Data) {
        var intent = Intent(bContext,UserEnquiryListActivity::class.java)
        intent.putExtra("cameFrom","admin")
        intent.putExtra("user",data as Serializable)
        startActivity(intent)
    }
}