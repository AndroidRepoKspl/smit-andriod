package com.kspl.smitglobal.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kspl.smitglobal.ApplicationClass.BaseActivity
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.CategoryAdaptor
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivityCategoryBinding
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.utils.Constants
import kotlinx.android.synthetic.main.activity_category.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategoryActivity : BaseActivity(), CategoryAdaptor.OnCategoryClick, SearchView.OnQueryTextListener {

    lateinit var binding: ActivityCategoryBinding
    var categoryList: MutableList<Category> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category)

        reloadCategories()

        iv_sync.setOnClickListener { reloadCategories() }

        iv_menu.setOnClickListener { finish() }
        binding.searchView2.isIconified = false
        binding.searchView2.onActionViewExpanded()


        val id = binding.searchView2.context.resources.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = binding.searchView2.findViewById(id) as TextView
        textView.setTextColor(Color.WHITE)
        textView.setHintTextColor(resources.getColor(R.color.white))
        textView.setBackgroundColor(resources.getColor(R.color.transpernt))

        binding.searchView2.isFocusable = true
        binding.searchView2.requestFocusFromTouch()
        binding.searchView2.clearFocus()
        binding.searchView2.setOnQueryTextListener(this)
        binding.searchView2.setOnClickListener { binding.searchView2.isIconified = false }

        updateSearchList(object : CategoryActivity.SearchList {
            override fun search(text: String) {
                val tempList: MutableList<Category> = ArrayList()
                val list = categoryList
                for (i in list.indices) {
                    if (list[i].name.toLowerCase().contains(text.toLowerCase())) {
                        tempList.add(list[i])
                    }
                }
                binding.rvCategories.layoutManager = LinearLayoutManager(bContext)
                binding.rvCategories.adapter = CategoryAdaptor(tempList, this@CategoryActivity)
            }
        })


    }

    fun reloadCategories() {
        Constants.showProgressDialog(bContext, "Loading All Category", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            callCategoryListApi()
        }
    }


    private suspend fun callCategoryListApi() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllCategories(Constants.getHeadersWithToken())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: CategoryResponse? = response.body()
                            sendDataToMainThread(data)

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private suspend fun sendDataToMainThread(data: CategoryResponse?) {

        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.categories.isEmpty()) {
                sendMsgToMainThread("No Data Found")
                binding.rvCategories.visibility = View.GONE
                binding.tvNoDataFound.visibility = View.VISIBLE
            } else {
                categoryList = data.categories as MutableList<Category>
                binding.rvCategories.visibility = View.VISIBLE
                binding.tvNoDataFound.visibility = View.GONE
                binding.rvCategories.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    setItemViewCacheSize(data.categories.size)
                    adapter = CategoryAdaptor(
                        data.categories,
                        this@CategoryActivity
                    )
                }
            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(bContext, it, Toast.LENGTH_LONG).show() }
            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun oncategoryclick(cat: Category) {
        val intent = Intent(bContext, SubCategoryActivity::class.java)
        intent.putExtra("name", cat.name)
        intent.putExtra("id", cat.id.toString())
        startActivity(intent)
    }

    override fun oncategorydelete(categoryResponse: Category) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Delete ${categoryResponse.name} ?")
        builder.setMessage("Are you sure you want to delete this category?\nNote: If you delete category then sub-categories & products Mapped to that category will also be deleted")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                deleteCategory(categoryResponse!!)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun deleteCategory(categoryResponse: Category) {
        Constants.showProgressDialog(
            bContext,
            "Deleting Category", "Please Wait..."
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response =
                    ApiClient.apiService.deleteCategory(
                        Constants.getHeadersWithToken(),
                        categoryResponse.id
                    )
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        deleteCategoryMsg("Successfully Deleted")
                    } else {
                        deleteCategoryMsg("Something went wrong")
                    }
                } else {
                    deleteCategoryMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                deleteCategoryMsg(e.message)
            }
        }

    }

    private suspend fun deleteCategoryMsg(msg: String?) {
        withContext(Dispatchers.Main) {
            Constants.dismiss()
            Toast.makeText(bContext, msg, Toast.LENGTH_SHORT).show()
            reloadCategories()
        }
    }


    override fun onQueryTextSubmit(s: String?): Boolean = false


    override fun onQueryTextChange(s: String?): Boolean {
        searchList.search(s!!)
        return false
    }

    private lateinit var searchList: SearchList

    fun updateSearchList(listener: SearchList) {
        searchList = listener

    }

    interface SearchList {
        fun search(text: String)
    }


}