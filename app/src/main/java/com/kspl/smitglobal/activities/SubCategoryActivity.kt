package com.kspl.smitglobal.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.adaptors.SubCategoryAdapter
import com.kspl.smitglobal.R
import com.kspl.smitglobal.adaptors.CategoryAdaptor
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.ActivitySubCategoryBinding
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategory
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategoryResponse
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SubCategoryActivity : AppCompatActivity(),SubCategoryAdapter.SubCategoryClick, SearchView.OnQueryTextListener  {
    var name:String? = null
    var catid:String? = null
    lateinit var  binding: ActivitySubCategoryBinding
    lateinit var context: Context
    private var subCategoryList: List<SubCategory> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_sub_category)
        context = this
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )


        if(intent.hasExtra("name")){
            name=intent.getStringExtra("name")
            binding.textView.text = name
        }
        if(intent.hasExtra("id")){
            catid = intent.getStringExtra("id")
        }

        if (PowerPreference.getDefaultFile().getString("role_id") == "2") {
            binding.ivFav.visibility = View.GONE
        } else {
            binding.ivFav.visibility = View.VISIBLE
        }
        binding.ivSync.setOnClickListener {
            reloadSubcategories()
        }

        binding.ivFav.setOnClickListener {
            startActivity(Intent(context,SendToEnquiryActivity::class.java))
        }

        binding.ivMenu.setOnClickListener { finish() }






        reloadSubcategories()

        binding.searchView3.isIconified = false
        binding.searchView3.onActionViewExpanded()

        binding.searchView3.isFocusable = true
        binding.searchView3.requestFocusFromTouch()
        binding.searchView3.clearFocus()
        binding.searchView3.setOnQueryTextListener(this)
        binding.searchView3.setOnClickListener { binding.searchView3.isIconified = false }


        val id = binding.searchView3.context.resources.getIdentifier("android:id/search_src_text", null, null)
        val textView = binding.searchView3.findViewById(id) as TextView
        textView.setTextColor(Color.WHITE)
        textView.setHintTextColor(resources.getColor(R.color.white))
        textView.setBackgroundColor(resources.getColor(R.color.transpernt))

        updateSearchList(object : SearchList {
            override fun search(text: String) {
                val tempList: MutableList<SubCategory> = ArrayList()
                val list = subCategoryList
                for(i in list.indices){
                    if(list[i].name.toLowerCase().contains(text.toLowerCase())){
                        tempList.add(list[i])
                    }
                }
                binding.rvCategories.layoutManager = LinearLayoutManager(context)
                binding.rvCategories.adapter = SubCategoryAdapter(tempList,this@SubCategoryActivity)
            }
        })
    }

    fun reloadSubcategories(){
        Constants.showProgressDialog(context, "Loading All Sub Category", "Please Wait")
        CoroutineScope(Dispatchers.IO).launch {
            callSubCategoryListApi()
        }
    }

    private suspend fun callSubCategoryListApi() {
        CoroutineScope(Dispatchers.IO).launch {
            if (Constants.isNetworkAvailable()) {
                try {
                    val response =
                        ApiClient.apiService.getAllSubCategory(Constants.getHeadersWithToken(),
                            catid.toString()
                        )
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val data: SubCategoryResponse? = response.body()
                            if(data!!.status == "success"){
                                sendDataToMainThread(data)
                            }else
                            {
                                sendMsgToMainThread(data.status)
                            }

                        } else {
                            sendMsgToMainThread(Constants.NULL_RESPONSE)
                        }
                    } else {
                        sendMsgToMainThread(response.message())
                    }

                } catch (e: Exception) {
                    sendMsgToMainThread(e.message)
                }
            } else {
                sendMsgToMainThread(Constants.NO_INTERNET)
            }
        }

    }

    private suspend fun sendDataToMainThread(data: SubCategoryResponse?) {

        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            if (data!!.data.sub_categories.isEmpty()) {
                binding.rvCategories.visibility = View.GONE
                binding.tvNoDataFound.visibility = View.VISIBLE
            } else {
                binding.rvCategories.visibility = View.VISIBLE
                binding.tvNoDataFound.visibility = View.GONE
                
                subCategoryList = data.data.sub_categories

                binding.rvCategories.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    setHasFixedSize(true)
                    setItemViewCacheSize(data.data.sub_categories.size)
                    adapter = SubCategoryAdapter(data.data.sub_categories,this@SubCategoryActivity)
                }
            }
        }

    }

    private suspend fun sendMsgToMainThread(message: String?) {
        withContext(Dispatchers.Main)
        {
            Constants.dismiss()
            message?.let { Toast.makeText(context, it, Toast.LENGTH_LONG).show() }
            //  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSubCategoryClick(subCat: SubCategory) {
        val intent = Intent(context,ProductActivity::class.java)
        intent.putExtra("name",subCat.name)
        intent.putExtra("id",subCat.id.toString())
        startActivity(intent)
    }

    override fun onSubCategoryDeleteClick(subCat: SubCategory) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Delete ${subCat.name} ?")
        builder.setMessage("Are you sure you want to delete this Sub-category?\nNote: If you delete sub-category then product Mapped to that sub-category will also be deleted")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                deleteSubCategory(subCat!!)
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun deleteSubCategory(subCat: SubCategory) {
        Constants.showProgressDialog(
            context,
            "Deleting Sub Category", "Please Wait..."
        )
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.apiService.deleteSubCategory(Constants.getHeadersWithToken(), subCat.id)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        deleteSubCategoryMsg("Successfully Deleted")
                    } else {
                        deleteSubCategoryMsg("Something went wrong")
                    }
                } else {
                    deleteSubCategoryMsg("Something went wrong")
                }
            } catch (e: java.lang.Exception) {
                deleteSubCategoryMsg(e.message)
            }
        }

    }

    private suspend fun deleteSubCategoryMsg(msg: String?) {
        withContext(Dispatchers.Main) {
            Constants.dismiss()
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            reloadSubcategories()

        }
    }

    override fun onQueryTextSubmit(s: String?): Boolean  = false

    override fun onQueryTextChange(s: String?): Boolean {
        searchList.search(s!!)
        return false
    }

    private lateinit var searchList: SearchList
    fun updateSearchList(listener: SearchList) {
        searchList = listener

    }
    interface SearchList {
        fun search(text: String)
    }
}