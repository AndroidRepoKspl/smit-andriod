package com.kspl.smitglobal.modelClasses.addEnquiry

import com.kspl.smitglobal.modelClasses.getEnquiries.DataX

data class AddEnquiryResponse(
    val `data`: List<DataX>,
    val message: String,
    val status: String?
)