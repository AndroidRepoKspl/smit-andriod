package com.kspl.smitglobal.modelClasses.fileSystem.getfiles

data class GetFiles(
    val `data`: List<Data>,
    val status: String,
    val root: String,
    val root_name: String,
    val parent_id: String
)