package com.kspl.smitglobal.modelClasses.EnquiryProductsResponse

data class Data(
    val category_id: Int,
    val company_name: String,
    val country_name: String,
    val created_at: String,
    val created_by: Int,
    val description: Any,
    val file_path: String,
    val grade_id: Int,
    val id: Int,
    val is_active: String,
    val is_seasonal: String,
    val multiple_images: List<Any>,
    val name: String,
    val previous_price: String,
    val price: String,
    val price_difference: String,
    val product_category: String,
    val product_grade: String,
    val sub_category_id: Int,
    val unit: String,
    val updated_at: String,
    val updated_by: Int
)