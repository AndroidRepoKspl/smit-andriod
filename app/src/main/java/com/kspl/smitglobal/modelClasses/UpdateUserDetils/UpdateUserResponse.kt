package com.kspl.smitglobal.modelClasses.UpdateUserDetils

data class UpdateUserResponse(
    val message: String,
    val status: String
)