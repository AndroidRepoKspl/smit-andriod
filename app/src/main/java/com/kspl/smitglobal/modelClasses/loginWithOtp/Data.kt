package com.kspl.smitglobal.modelClasses.loginWithOtp

data class Data(
    val message: String,
    val mobile_no: String,
    val otp: Int,
    val register: Boolean
)