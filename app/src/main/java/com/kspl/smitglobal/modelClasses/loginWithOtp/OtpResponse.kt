package com.kspl.smitglobal.modelClasses.loginWithOtp

data class OtpResponse (
    val `data`: Data,
    val status: String,
    val message: String
)