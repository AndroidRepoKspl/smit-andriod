package com.kspl.smitglobal.modelClasses.UsersResponse

import java.io.Serializable

data class Data(
    val created_at: String,
    val device_type: String,
    val email: String,
    val email_verified_at: Any,
    val id: Int,
    val mobile_no: String,
    val name: String,
    val otp: Any,
    val otp_expires_at: Any,
    val role_id: Int,
    val updated_at: String,
    val wrong_login_attempt: Any
):Serializable