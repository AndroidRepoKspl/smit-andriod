package com.kspl.smitglobal.modelClasses.getEnquiries

data class DataX(
    val category_id: Int,
    val created_at: String,
    val created_by: Int,
    val description: String?,
    val country_name: String,
    val company_name: String,
    val file_path: String,
    val grade_id: Int,
    val id: Int,
    val in_enquiry_cart: Boolean,
    val is_active: String,
    val is_seasonal: String,
    val multiple_images: List<String>,
    val name: String,
    val previous_price: String,
    val price: String,
    val price_difference: String,
    val product_category: String,
    val product_grade: String,
    val sub_category_id: Int,
    val sub_category_name:String,
    val unit: String,
    val updated_at: String,
    val updated_by: Int,
    var isSelected: Boolean? = false
)