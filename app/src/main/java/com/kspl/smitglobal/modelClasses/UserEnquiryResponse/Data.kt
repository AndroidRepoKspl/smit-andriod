package com.kspl.smitglobal.modelClasses.UserEnquiryResponse

data class Data(
    var isSelected : Boolean = false,
    val created_at: String,
    val created_by: Int,
    val enquiry_status: Int,
    val id: Int,
    val updated_at: String
)