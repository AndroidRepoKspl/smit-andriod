package com.kspl.smitglobal.modelClasses.products

data class ProductsResponse(
    val categories: List<Category>,
    val grades: List<Grade>,
    val products: List<Product>
)