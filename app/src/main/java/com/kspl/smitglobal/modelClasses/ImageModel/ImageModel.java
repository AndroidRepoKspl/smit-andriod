package com.kspl.smitglobal.modelClasses.ImageModel;

public class ImageModel {

    private String imageName;
    private String imageLocation;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }
}
