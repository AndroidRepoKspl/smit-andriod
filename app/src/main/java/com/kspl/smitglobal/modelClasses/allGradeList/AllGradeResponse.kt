package com.kspl.smitglobal.modelClasses.allGradeList

data class AllGradeResponse(
    val `data`: Data,
    val status: String
)