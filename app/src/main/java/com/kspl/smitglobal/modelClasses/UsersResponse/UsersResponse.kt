package com.kspl.smitglobal.modelClasses.UsersResponse

data class UsersResponse(
    val `data`: List<Data>,
    val message: String,
    val status: String
)