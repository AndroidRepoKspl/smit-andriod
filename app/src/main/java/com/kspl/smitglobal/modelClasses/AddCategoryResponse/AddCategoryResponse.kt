package com.kspl.smitglobal.modelClasses.AddCategoryResponse

data class AddCategoryResponse(
    val message: String,
    val status: String
)