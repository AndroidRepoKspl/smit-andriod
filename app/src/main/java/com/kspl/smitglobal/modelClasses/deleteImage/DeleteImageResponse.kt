package com.kspl.apmcmarket.models.deleteImage

data class DeleteImageResponse(
    val `data`: Data,
    val message: String,
    val status: String
)