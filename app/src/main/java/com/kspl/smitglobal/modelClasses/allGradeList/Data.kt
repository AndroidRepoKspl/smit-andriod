package com.kspl.smitglobal.modelClasses.allGradeList

data class Data(
    val product_grades: List<ProductGrade>
)