package com.kspl.smitglobal.modelClasses.CategoryResponse

data class CategoryResponse(
    val categories: List<Category>,
    val message:String
)