package com.kspl.smitglobal.modelClasses.getEnquiries

data class GetEnquiresResponse(
    val `data`: List<DataX>,
    val message: String,
    val status: String
)