package com.kspl.smitglobal.modelClasses.SubCategoryResponse

data class Data(
    val sub_categories: List<SubCategory>
)