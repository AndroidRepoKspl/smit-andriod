package com.kspl.smitglobal.modelClasses.EnquiryProductsResponse

data class EnquiryProductsResponse(
    val `data`: List<Data>,
    val message: String,
    val status: String
)