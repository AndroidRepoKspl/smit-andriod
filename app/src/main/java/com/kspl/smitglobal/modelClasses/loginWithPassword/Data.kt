package com.kspl.smitglobal.modelClasses.loginWithPassword

data class Data(
    val exception: Any,
    val headers: Headers,
    val original: Original,
    val access_token: String,
    val token_type: String,
    val expires_at: String
)