package com.kspl.smitglobal.modelClasses.GradeResponse

data class GetGradeResponse(
    val `data`: Data,
    val status: String
)