package com.kspl.smitglobal.modelClasses.deleteEnquiry

data class DeleteEnquiryResponse(
    val `data`: List<Any>,
    val message: String,
    val status: String
)