package com.kspl.smitglobal.modelClasses.SubCategoryResponse

data class SubCategoryResponse(
    val `data`: Data,
    val status: String
)