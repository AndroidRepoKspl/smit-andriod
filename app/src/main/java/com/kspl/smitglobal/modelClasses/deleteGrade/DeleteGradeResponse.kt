package com.kspl.smitglobal.modelClasses.deleteGrade

data class DeleteGradeResponse(
    val message: String,
    val status: String
)