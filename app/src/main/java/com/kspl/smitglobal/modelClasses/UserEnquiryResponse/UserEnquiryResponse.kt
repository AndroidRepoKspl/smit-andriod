package com.kspl.smitglobal.modelClasses.UserEnquiryResponse

data class UserEnquiryResponse(
    val `data`: List<Data>,
    val message: String,
    val status: String
)