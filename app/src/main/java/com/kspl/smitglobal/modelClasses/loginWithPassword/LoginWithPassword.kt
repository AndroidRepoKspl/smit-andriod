package com.kspl.smitglobal.modelClasses.loginWithPassword

data class LoginWithPassword(
    val `data`: Data,
    val status: String,
    val message: String?,
    val user: User
)