package com.kspl.smitglobal.modelClasses.fileSystem.getfiles

data class Data(
    val created_at: String,
    val file_name: String,
    val id: Int,
    val link: String,
    val parent_id: Int,
    val status: Int,
    val updated_at: String
)