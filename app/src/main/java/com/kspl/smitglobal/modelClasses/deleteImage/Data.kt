package com.kspl.apmcmarket.models.deleteImage

data class Data(
    val multiple_images: List<String>
)