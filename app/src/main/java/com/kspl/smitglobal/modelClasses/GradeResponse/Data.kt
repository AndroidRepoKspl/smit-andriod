package com.kspl.smitglobal.modelClasses.GradeResponse

data class Data(
    val product_grades: List<ProductGrade>
)