package com.kspl.smitglobal.modelClasses.products

data class Category(
    val created_at: String,
    val created_by: Int,
    val gu_name: Any,
    val hi_name: Any,
    val id: Int,
    val mr_name: Any,
    val name: String,
    val updated_at: String,
    val updated_by: Int
)