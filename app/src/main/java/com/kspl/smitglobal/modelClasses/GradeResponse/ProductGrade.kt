package com.kspl.smitglobal.modelClasses.GradeResponse

data class ProductGrade(
    val created_at: String,
    val created_by: Int,
    val id: Int,
    val name: String,
    val updated_at: String,
    val updated_by: Int
)