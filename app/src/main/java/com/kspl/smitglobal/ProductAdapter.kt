package com.kspl.smitglobal

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.devs.readmoreoption.ReadMoreOption
import com.kspl.smitglobal.ProductAdapter.ProductViewHolder
import com.kspl.smitglobal.api.ApiClient.Companion.IMG_URL
import com.kspl.smitglobal.databinding.SingleItemLayoutBinding
import com.kspl.smitglobal.modelClasses.products.Product
import com.kspl.smitglobal.utils.Constants.Companion.isSelectionModeOn
import com.preference.PowerPreference
import com.preference.provider.PreferenceProvider
import com.squareup.picasso.Picasso
import java.util.*

class ProductAdapter(private var list: List<Product>,
                     private val listener: ProductClickListener,
                     private val deletelistener: deleteProduct,
                     private val updatelistener: editProduct,
                     private val detailsClick: ProductDetails) :
    RecyclerView.Adapter<ProductViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        context = parent.context
        val binding: SingleItemLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.single_item_layout, parent, false
        )
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindProduct(list[position])
    }

    val selectedProducts: List<Product>
        get() {
            val selectedProduct: MutableList<Product> = ArrayList()
            for (product in list) {
                if(product.isSelected==null){
                    product.isSelected = false
                }
                if (product.isSelected!!) {
                    selectedProduct.add(product)
                }
            }
            return selectedProduct
        }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(newProductList: List<Product>) {
        list = newProductList
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(var binding: SingleItemLayoutBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindProduct(product: Product?) {
            if(product?.isSelected==null){
                product?.isSelected = false
            }

            if(isSelectionModeOn){
                if(!product!!.in_enquiry_cart){
                    binding.ivSelected.visibility = View.VISIBLE
                }
            }else{
                binding.ivSelected.visibility = View.INVISIBLE
            }

            if(product!!.in_enquiry_cart){
                binding.tvSendEnquiry.text = "Already added"
                binding.tvSendEnquiry.setBackgroundResource(R.drawable.btn_bg)
                binding.tvSendEnquiry.setTextColor(context.resources.getColor(R.color.white))
                binding.tvSendEnquiry.isEnabled = false
            }else
            {
                binding.tvSendEnquiry.text = "Send Inquiry"
                binding.tvSendEnquiry.isEnabled = true
                binding.tvSendEnquiry.setBackgroundResource(R.drawable.btn_send_inquiry)
                binding.tvSendEnquiry.setTextColor(context.resources.getColor(R.color.blue))
            }

            binding.tvItemName.text = product?.name


            binding.tvItemPrice.text = "\u20B9${product!!.price} / (${product.unit} Unit)"

            if(product?.multiple_images!=null){
                if(product.multiple_images.isNotEmpty()){
                    updateWithUrl(IMG_URL+product.multiple_images[0])
                }else{
                    updateWithUrl(product.file_path)
                }
            }

            if (product?.description == null) {
                binding.tvDescription.visibility = View.GONE
            } else {
                binding.tvDescription.visibility = View.VISIBLE
                val readMoreOption = ReadMoreOption.Builder(PreferenceProvider.context)
                    .textLength(1, ReadMoreOption.TYPE_LINE) // OR
                    //.textLength(3, ReadMoreOption.TYPE_CHARACTER)
                    .moreLabel("Read more")
                    .lessLabel("Read less")
                    .moreLabelColor(Color.WHITE)
                    .lessLabelColor(Color.WHITE)
                    .labelUnderLine(false)
                    .expandAnimation(true)
                    .build()
                readMoreOption.addReadMoreTo(
                    binding.tvDescription,
                    "${product.description}  "
                )
            }

            if(product!!.product_grade ==null){
                binding.tvGarde.visibility = View.GONE
            }else
            {
                binding.tvGarde.visibility = View.VISIBLE
                binding.tvGarde.text = "Grade : ${product.product_grade}"
            }

            if(product.company_name ==null){
                binding.tvCompanyName.visibility = View.GONE
            }else
            {
                binding.tvCompanyName.visibility = View.VISIBLE
                binding.tvCompanyName.text = "Company Name : ${product.company_name}"
            }

      /*      if(product.country_name ==null){
                binding.tvCountryName.visibility = View.GONE
            }else
            {
                binding.tvCountryName.visibility = View.VISIBLE
                binding.tvCountryName.text = "Country : ${product.country_name}"
            }
*/


            if (PowerPreference.getDefaultFile().getString("role_id") == "2") {
                binding.layoutEditDelete.visibility = View.VISIBLE
                binding.tvSendEnquiry.visibility=View.GONE
            } else {
                binding.layoutEditDelete.visibility = View.GONE
                binding.tvSendEnquiry.visibility=View.VISIBLE

            }

            binding.btnDelete.setOnClickListener {
                product?.let { it1 -> deletelistener.deleteProductClick(it1) }
            }

            binding.btnEdit.setOnClickListener {
                product?.let { it1 -> updatelistener.editProductClick(it1) }
            }


            if(product?.isSelected!=null){
                if (product?.isSelected!!) {
                    binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.selected_bg))
                 //   binding.ivSelected.visibility = View.VISIBLE
                } else {
                    binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.bg_unselected_round))
               //     binding.ivSelected.visibility = View.INVISIBLE
                }
            }else
            {
                binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.bg_unselected_round))
             //   binding.ivSelected.visibility = View.INVISIBLE
            }

            if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
                itemView.setOnLongClickListener(OnLongClickListener { view: View? ->
                    if (!isSelectionModeOn) {
                        if(!product.in_enquiry_cart){
                            if (!product?.isSelected!!) {
                                binding.ivSelected.visibility = View.VISIBLE
                                binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.selected_bg))
                                product?.isSelected = true
                                isSelectionModeOn = true
                                listener.onProductClickAction(true)
                                notifyDataSetChanged()
                            }
                        }

                    }

                    true
                })
            }




            itemView.setOnClickListener { view: View? ->
                if (isSelectionModeOn) {
                    if(!product!!.in_enquiry_cart){
                        if (product?.isSelected!!) {
                            //    binding.ivSelected.visibility = View.INVISIBLE
                            binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.bg_unselected_round))
                            product?.isSelected = false
                            if (selectedProducts.isEmpty()) {
                                listener.onProductClickAction(false)
                            }
                        } else {
                            //   binding.ivSelected.visibility = View.VISIBLE
                            binding.ivSelected.setImageDrawable(context.resources.getDrawable(R.drawable.selected_bg))
                            product?.isSelected = true
                            listener.onProductClickAction(true)
                        }
                    }

                }else{

                    detailsClick.productDetailsClick(product)
                }
            }

            binding.cvIvItem.setOnClickListener {
                listener.onImageClicked(product)
            }

            binding.tvSendEnquiry.setOnClickListener {
                listener.onEnquiryClick(product)
            }
        }

        private fun updateWithUrl(url: String) {
            Picasso.get()
                .load(url)
                .error(R.drawable.square_logo)
                .placeholder(R.drawable.square_logo)
                .into(binding.ivItemImage)
        }
    }


    interface deleteProduct{
        fun deleteProductClick(product: Product)
    }
    interface editProduct{
        fun editProductClick(product: Product)
    }
    interface ProductDetails{
        fun productDetailsClick(product: Product)
    }

}