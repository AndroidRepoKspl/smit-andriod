package com.kspl.smitglobal.api

import com.kspl.apmcmarket.models.deleteImage.DeleteImageResponse
import com.kspl.smitglobal.modelClasses.AddCategoryResponse.AddCategoryResponse
import com.kspl.smitglobal.modelClasses.loginWithOtp.OtpResponse
import com.kspl.smitglobal.modelClasses.loginWithPassword.LoginWithPassword
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.kspl.smitglobal.modelClasses.EnquiryProductsResponse.EnquiryProductsResponse
import com.kspl.smitglobal.modelClasses.GradeResponse.GetGradeResponse
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategoryResponse
import com.kspl.smitglobal.modelClasses.products.ProductsResponse
import com.kspl.smitglobal.modelClasses.UpdateUserDetils.UpdateUserResponse
import com.kspl.smitglobal.modelClasses.UserEnquiryResponse.UserEnquiryResponse
import com.kspl.smitglobal.modelClasses.UsersResponse.UsersResponse
import com.kspl.smitglobal.modelClasses.addEnquiry.AddEnquiryResponse
import com.kspl.smitglobal.modelClasses.allGradeList.AllGradeResponse
import com.kspl.smitglobal.modelClasses.deleteEnquiry.DeleteEnquiryResponse
import com.kspl.smitglobal.modelClasses.deleteGrade.DeleteGradeResponse
import com.kspl.smitglobal.modelClasses.fileSystem.getfiles.GetFiles
import com.kspl.smitglobal.modelClasses.getEnquiries.GetEnquiresResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {


    @POST("getloginOTP")
    @FormUrlEncoded
    suspend fun getOtp(
        @HeaderMap headers: Map<String, String>,
        @Field("mobile_no") mobile_no: String
    ): Response<OtpResponse>

    @POST("loginWithOTP")
    @FormUrlEncoded
    suspend fun verify(
        @HeaderMap headers: Map<String, String>,
        @Field("name") name: String?,
        @Field("mobile_no") mobile_no: String,
        @Field("otp") otp: String,
        @Field("password") password: String?,
        @Field("email") email: String?
    ): Response<LoginWithPassword>




    @POST("loginWithPassword")
    @FormUrlEncoded
    suspend fun loginWithPassword(
        @HeaderMap headers: Map<String, String>,
        @Field("user_name") user_name: String,
        @Field("password") password: String,
        @Field("device_type") device_type: String = "Android"
    ): Response<LoginWithPassword>


    @GET("file-system")
    suspend fun getFiles(
        @HeaderMap headers: Map<String, String>,
        @Query("parent_id") parent_id: String?,
        @Query("root_id") root_id: String?,
        @Query("root_name") root_name: String?
    ): Response<GetFiles>

    @POST("get-previous-folder-list")
    @FormUrlEncoded
    suspend fun onBackApi(
        @HeaderMap headers: Map<String, String>,
        @Field("root_id") root_id: String?,
        @Field("root_name") root_name: String?
    ): Response<GetFiles>

    @GET("file-system")
    suspend fun getRootDirectory(
        @HeaderMap headers: Map<String, String>
    ): Response<GetFiles>


    @POST("file-system")
    @FormUrlEncoded
    suspend fun addFileFolder(
        @HeaderMap headers: Map<String, String>,
        @Field("file_name") file_name: String?,
        @Field("parent_id") parent_id: String?,
        @Field("link") link: String?,
        @Field("status") status: String?
    ): Response<AddCategoryResponse>


    @DELETE("file-system/{id}")
    suspend fun deleteFile(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<AddCategoryResponse>


    @POST("enquiry-cart")
    @FormUrlEncoded
    suspend fun addEnquiry(
        @HeaderMap headers: Map<String, String>,
        @Field("product_ids") product_ids: String
    ): Response<AddEnquiryResponse>

    @GET("enquiry-cart")
    suspend fun getAllEnquiries(
        @HeaderMap headers: Map<String, String>
    ): Response<GetEnquiresResponse>

    @GET("productGrade")
    suspend fun getAllGradeList(
        @HeaderMap headers: Map<String, String>
    ): Response<GetGradeResponse>

    @GET("enquiry-users-list")
    suspend fun getAllUserList(
        @HeaderMap headers: Map<String, String>
    ): Response<UsersResponse>

    @POST("deleteImage")
    @FormUrlEncoded
    suspend fun deleteImage(
        @HeaderMap headers: Map<String, String>,
        @Field("product_id") product_id: String,
        @Field("product_image") product_image: String
    ): Response<DeleteImageResponse>


    @POST("get-user-enquiry-list")
    @FormUrlEncoded
    suspend fun getUserEnquiryList(
        @HeaderMap headers: Map<String, String>,
        @Field("user_id") product_id: String
    ): Response<UserEnquiryResponse>

    @POST("get-enquiry-details")
    @FormUrlEncoded
    suspend fun getEnquiryProdctList(
        @HeaderMap headers: Map<String, String>,
        @Field("enquiry_id") enquiry_id: String
    ): Response<EnquiryProductsResponse>




//      @POST("updateProfile")
//      @FormUrlEncoded
//      suspend fun updateProfile(
//          @HeaderMap headers: Map<String, String>,
//          @Field("user_id") user_id: String,
//          @Field("name") name: String,
//          @Field("password") password: String?,
//          @Field("email") email: String
//      ): Response<UpdateProfileResponse>

//    @POST("register_new")
//    @FormUrlEncoded
//    suspend fun register(
//        @HeaderMap headers: Map<String, String>,
//        @Field("email") email: String,
//        @Field("password") password: String,
//        @Field("name") name: String,
//        @Field("c_password") c_password: String,
//        @Field("mobile_no") mobile_no: String
//    ): Response<RegisterResponse>


    @GET("product")
    suspend fun getroducts(
        @HeaderMap headers: Map<String, String>,
        @Query("sub_category_id") searchKey: String?
    ): Response<ProductsResponse>

    @GET("category")
    suspend fun getAllCategories(
        @HeaderMap headers: Map<String, String>
    ): Response<CategoryResponse>

    @POST("category")
    @FormUrlEncoded
    suspend fun addCategory(
        @HeaderMap headers: Map<String, String>,
        @Field("category_name") category_name: String
    ): Response<AddCategoryResponse>


    @POST("productGrade")
    @FormUrlEncoded
    suspend fun addGrade(
        @HeaderMap headers: Map<String, String>,
        @Field("grade_name") grade_name: String
    ): Response<AddCategoryResponse>

    @GET("sub-category-by-category/{id}")
    suspend fun getAllSubCategory(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: String
    ): Response<SubCategoryResponse>


    @POST("sub-category")
    @FormUrlEncoded
    suspend fun addSubCategory(
        @HeaderMap headers: Map<String, String>,
        @Field("sub_category_name") sub_category_name: String,
        @Field("category_id") category_id: String
    ): Response<AddCategoryResponse>

    @DELETE("category/{id}")
    suspend fun deleteCategory(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<Unit>

    @DELETE("sub-category/{id}")
    suspend fun deleteSubCategory(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<Unit>

    @DELETE("product/{id}")
    suspend fun deleteProduct(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<Unit>

    @DELETE("enquiry-cart/{id}")
    suspend fun deleteEnquiryProduct(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<DeleteEnquiryResponse>

    @DELETE("productGrade/{id}")
    suspend fun deleteGrade(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id: Int
    ): Response<DeleteGradeResponse>


    @POST("updateProfile")
    @FormUrlEncoded
    suspend fun updateProfile(
        @HeaderMap headers: Map<String, String>,
        @Field("user_id") user_id: String,
        @Field("name") name: String,
        @Field("password") password: String,
        @Field("email") email: String

    ): Response<UpdateUserResponse>


    @GET("submit-enquiry-cart")
    suspend fun submitEnquiry(
        @HeaderMap headers: Map<String, String>,
    ): Response<DeleteEnquiryResponse>


//
//    @Multipart
//    @POST("product")
//    suspend fun addProduct(
//        @HeaderMap headers: Map<String, String>,
//        @Part("product_name") product_name: String,
//        @Part("product_price") product_price: String,
//        @Part("product_unit") product_unit: String,
//        @Part("category_name") category_name: String,
//        @Part("grade_name") grade_name: String,
//        @Part("product_image") product_image: MultipartBody.Part
//    ): Response<AddProductResponse>


//    @Multipart
//    @PUT("product/{id}")
//    suspend fun updateProduct(
//        @HeaderMap headers: Map<String, String>,
//        @Path("id") id: Int,
//        @Part("product_name") product_name: String,
//        @Part("product_price") product_price: String,
//        @Part("product_id") product_id: String,
//        @Part("category_name") category_name: String,
//        @Part("grade_name") grade_name: String,
//        @Part("product_image") product_image: MultipartBody.Part
//    ): Response<AddProductResponse>


//
//
//      @GET("inactiveProduct")
//      suspend fun getinactiveProducts(
//          @HeaderMap headers: Map<String, String>
//      ): Response<InactiveProductsResponse>
//
//      @POST("productActiveInactive")
//      @FormUrlEncoded
//      suspend fun activeProduct(
//          @HeaderMap headers: Map<String, String>,
//          @Field("product_id") product_id: String,
//          @Field("status") status: String
//      ): Response<ActiveProductResponse>
//
//      @POST("deleteImage")
//      @FormUrlEncoded
//      suspend fun deleteImage(
//          @HeaderMap headers: Map<String, String>,
//          @Field("product_id") product_id: String,
//          @Field("product_image") product_image: String
//      ): Response<DeleteImageResponse>


}