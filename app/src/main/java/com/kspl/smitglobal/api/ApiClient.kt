package com.kspl.smitglobal.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object{
        //   const val BASE_URL: String = "http://inneripro.com/apmc-market/public/api/"
   //     const val BASE_URL: String = "https://inneripro.com/apmc_test/public/api/"
        const val BASE_URL: String = "https://inneripro.com/smit/public/api/"

        // const val IMG_URL: String = "http://inneripro.com/apmc-market/public/uploads/"
        const val IMG_URL: String = "https://inneripro.com/smit/public/uploads/"


        private val retrofitBuilder: Retrofit.Builder by lazy {


            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .addNetworkInterceptor(StethoInterceptor())
                .build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)

        }

        val apiService: ApiService by lazy {
            retrofitBuilder
                .build()
                .create(ApiService::class.java)
        }
    }
}