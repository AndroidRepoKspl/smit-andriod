package com.kspl.smitglobal.ApplicationClass

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.kspl.smitglobal.R
import com.kspl.smitglobal.utils.Constants.Companion.dismiss
import com.kspl.smitglobal.utils.Constants.Companion.getHeadersWithToken
import com.preference.PowerPreference

abstract class BaseActivity : AppCompatActivity() {
    lateinit var bContext: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bContext  = this
        PowerPreference.init(baseContext)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

    }

    fun dissmissDialog() = dismiss()

    fun toast(msg: String){
        Toast.makeText(bContext, msg, Toast.LENGTH_SHORT).show()
    }

    fun getString(key:String):String{
        return PowerPreference.getDefaultFile().getString(key,"")
    }

    fun getInt(key:String):Int{
        return PowerPreference.getDefaultFile().getInt(key)
    }

    fun setString(key: String,value:String){
        PowerPreference.getDefaultFile().setString(key,value)
    }

    fun setInt(key: String,value:Int){
        PowerPreference.getDefaultFile().setInt(key,value)
    }

    fun setBoolean(key: String,value: Boolean){
        PowerPreference.getDefaultFile().setBoolean(key,value)
    }

    fun getBoolean(key:String):Boolean{
        return PowerPreference.getDefaultFile().getBoolean(key)
    }

    fun isLoggedin():Boolean{
        return PowerPreference.getDefaultFile().getBoolean("isLogin")
    }

    fun clearPref(){
        PowerPreference.clearAllData()
    }
    fun headers() = getHeadersWithToken()


    fun headerWithToken() = getHeadersWithToken()
}