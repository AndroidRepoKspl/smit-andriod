package com.kspl.smitglobal.ApplicationClass

import android.content.Context
import android.os.StrictMode
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.preference.PowerPreference

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        PowerPreference.init(appContext)
        Stetho.initializeWithDefaults(this)

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


    companion object{
        lateinit var appContext : Context
    }
}