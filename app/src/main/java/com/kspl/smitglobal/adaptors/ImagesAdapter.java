package com.kspl.smitglobal.adaptors;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kspl.smitglobal.R;
import com.kspl.smitglobal.modelClasses.ImageModel.ImageModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.kspl.smitglobal.api.ApiClient.IMG_URL;


public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {
    private Context context;
    private List<ImageModel> data = new ArrayList<>();
    private LayoutInflater inflater;
    private deleteProductImage mlistener;

    public ImagesAdapter(Context context, deleteProductImage mlistener) {
        inflater = LayoutInflater.from(context);
        this.mlistener=mlistener;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.single_images_layout, parent, false);

        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ImageModel current = data.get(position);

        if(current.getImageLocation().contains("API_IMAGE")){
            String ImageUrl = IMG_URL+current.getImageName();
            Picasso.get()

                    .load(ImageUrl)
                    .error(R.drawable.logo)
                    .placeholder(R.drawable.logo)
                    .into(holder.imImage);
        }else
        {
            holder.imImage.setImageURI(Uri.parse(current.getImageLocation()));

        }

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlistener.onDeletePressed(current,position);
            }
        });



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imImage;
        ImageView tvDelete;
        //ImageView imCancel;

        MyViewHolder(View itemView) {
            super(itemView);
            imImage = (ImageView) itemView.findViewById(R.id.iv_vertical_image_rv);
            tvDelete = (ImageView) itemView.findViewById(R.id.tv_delete);

        }
    }

    public void addItem(ImageModel imagesModel) {
        data.add(imagesModel);
        notifyItemInserted(data.size());
    }

    public void removeItem(ImageModel imagesModel,int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }

    public List<String> getSelectedImages() {
        List<String> imageListToReturn = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            imageListToReturn.add(data.get(i).getImageName());
        }
        return imageListToReturn;
    }

    public interface deleteProductImage{
       void onDeletePressed(ImageModel imageModel, int postion);
    }
}
