package com.kspl.smitglobal.adaptors

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.devs.readmoreoption.ReadMoreOption
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.SingleEnquiryLayoutBinding
import com.kspl.smitglobal.databinding.SingleEnquiryProductBinding
import com.kspl.smitglobal.modelClasses.EnquiryProductsResponse.Data
import com.preference.provider.PreferenceProvider
import com.squareup.picasso.Picasso

class EnquiryProductAdaptor(var data: List<Data>? ): RecyclerView.Adapter<EnquiryProductAdaptor.MyViewHolder>() {

    private lateinit var context: Context

    class MyViewHolder(var binding: SingleEnquiryProductBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context

        val binding: SingleEnquiryProductBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_enquiry_product,
            parent, false
        )
        return EnquiryProductAdaptor.MyViewHolder(binding)
    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvItemName.text=data!![position].name
        holder.binding.tvItemPrice.text = "\u20B9${data!![position].price} / (${data!![position].unit} Unit)"

        if(data!![position].multiple_images!=null){
            if(data!![position].multiple_images.isNotEmpty()){
                updateWithUrl(ApiClient.IMG_URL +data!![position].multiple_images[0],holder.binding)
            }else{
                updateWithUrl(data!![position].file_path,holder.binding)
            }
        }

        if (data!![position]?.description == null) {
            holder.binding.tvDescription.visibility = View.GONE
        } else {
            holder.binding.tvDescription.visibility = View.VISIBLE
            val readMoreOption = ReadMoreOption.Builder(PreferenceProvider.context)
                .textLength(1, ReadMoreOption.TYPE_LINE) // OR
                //.textLength(3, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("Read more")
                .lessLabel("Read less")
                .moreLabelColor(Color.WHITE)
                .lessLabelColor(Color.WHITE)
                .labelUnderLine(false)
                .expandAnimation(true)
                .build()
            readMoreOption.addReadMoreTo(
                holder.binding.tvDescription,
                "${data!![position].description}  "
            )
        }

        if(data!![position].product_grade ==null){
            holder.binding.tvGarde.visibility = View.GONE
        }else
        {
            holder.binding.tvGarde.visibility = View.VISIBLE
            holder.binding.tvGarde.text = "Grade : ${data!![position].product_grade}"
        }

        if(data!![position].company_name ==null){
            holder.binding.tvCompanyName.visibility = View.GONE
        }else
        {
            holder.binding.tvCompanyName.visibility = View.VISIBLE
            holder.binding.tvCompanyName.text = "Company Name : ${data!![position].company_name}"
        }

      /*  if(data!![position].country_name ==null){
            holder.binding.tvCountryName.visibility = View.GONE
        }else
        {
            holder.binding.tvCountryName.visibility = View.VISIBLE
            holder.binding.tvCountryName.text = "Country : ${data!![position].country_name}"
        }*/

    }

    private fun updateWithUrl(url: String, binding: SingleEnquiryProductBinding) {
        Picasso.get()
            .load(url)
            .error(R.drawable.square_logo)
            .placeholder(R.drawable.square_logo)
            .into(binding.ivItemImage)
    }
    
}