package com.kspl.smitglobal.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.SingleCategoryLayoutBinding
import com.kspl.smitglobal.modelClasses.CategoryResponse.Category
import com.kspl.smitglobal.modelClasses.CategoryResponse.CategoryResponse
import com.preference.PowerPreference

class CategoryAdaptor(
    var data: List<Category>?,
    var listener: OnCategoryClick


) : RecyclerView.Adapter<CategoryAdaptor.MyViewHolder>() {
    private lateinit var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context

        val binding: SingleCategoryLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_category_layout,
            parent, false
        )
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvCategoryNumber.text = (position + 1).toString()
        holder.binding.tvCategoryName.text = data!!.get(position).name

        if(PowerPreference.getDefaultFile().getString("role_id") == "3"){
            holder.binding.ivDelete.visibility = View.GONE
        }else
        {
            holder.binding.ivDelete.visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener {
            listener.oncategoryclick(data!![position])
        }

        holder.binding.ivDelete.setOnClickListener {
            listener.oncategorydelete(data!![position])
        }

    }

    class MyViewHolder(var binding: SingleCategoryLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface OnCategoryClick {
        fun oncategoryclick(categoryResponse: Category)
        fun oncategorydelete(categoryResponse: Category)

    }

}