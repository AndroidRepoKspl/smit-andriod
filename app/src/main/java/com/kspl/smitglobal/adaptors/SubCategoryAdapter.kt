package com.kspl.smitglobal.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.SingleCategoryLayoutBinding
import com.kspl.smitglobal.modelClasses.SubCategoryResponse.SubCategory
import com.preference.PowerPreference

class SubCategoryAdapter(
    var data: List<SubCategory>?,
    var listener: SubCategoryClick
    ):RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder>() {
    private lateinit var context: Context


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        context = parent.context
        val binding: SingleCategoryLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_category_layout,
            parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvCategoryNumber.text= (position+1).toString()
        holder.binding.tvCategoryName.text= data!![position].name

        if(PowerPreference.getDefaultFile().getString("role_id") == "3"){
            holder.binding.ivDelete.visibility = View.GONE
        }else
        {
            holder.binding.ivDelete.visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener {
            listener.onSubCategoryClick(data!![position])
        }
        holder.binding.ivDelete.setOnClickListener {
            listener.onSubCategoryDeleteClick(data!![position])
        }
    }

    override fun getItemCount(): Int = data?.size ?: 0

    class MyViewHolder(var binding: SingleCategoryLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface SubCategoryClick{
        fun onSubCategoryClick(subCat: SubCategory)
        fun onSubCategoryDeleteClick(subCat: SubCategory)

    }
}