package com.kspl.smitglobal.adaptors

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.SingleFileLayoutBinding
import com.kspl.smitglobal.modelClasses.fileSystem.getfiles.Data
import com.preference.PowerPreference

class FilesAdapter(
    var list: List<Data>,
    var listener : FileCliked
): RecyclerView.Adapter<FilesAdapter.FilesViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilesViewHolder {
        val binding: SingleFileLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.single_file_layout,
            parent,false
        )
        return FilesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FilesViewHolder, position: Int) {
        val current = list[position]

        if (PowerPreference.getDefaultFile().getString("role_id") == "3") {
            holder.binding.ivItemDelete.visibility = View.GONE
        }else
        {
            holder.binding.ivItemDelete.visibility = View.VISIBLE
        }

        if(current.status==0){
           holder.binding.ivItemImage.setImageResource(R.drawable.folder_image)
        }else
        {
            holder.binding.ivItemImage.setImageResource(R.drawable.file_image)
        }
        holder.binding.tvFileName.text = current.file_name

        holder.binding.ivItemImage.setOnClickListener {
            listener.onFileClicked(current)
        }
        holder.binding.ivItemDelete.setOnClickListener {
            listener.onDeleteClicked(current)
        }
    }

    override fun getItemCount() = list.size

    class FilesViewHolder(var binding: SingleFileLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    interface FileCliked{
        fun onFileClicked(file : Data)
        fun onDeleteClicked(file: Data)
    }
}