package com.kspl.smitglobal.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.SingleEnquiryLayoutBinding

class EnqiryDetailsAdaptor(var data: List<com.kspl.smitglobal.modelClasses.UserEnquiryResponse.Data>?,var listener:EnquiryClick) :RecyclerView.Adapter<EnqiryDetailsAdaptor.MyViewHolder>() {
    private lateinit var context: Context


    class MyViewHolder(var binding: SingleEnquiryLayoutBinding): RecyclerView.ViewHolder(binding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context

        val binding: SingleEnquiryLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_enquiry_layout,
            parent, false
        )
        return EnqiryDetailsAdaptor.MyViewHolder(binding)    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.ENo.text= "Enquiry id: ${data!![position].id.toString()}"
        holder.binding.EDate.text= data!![position].created_at


        setView(holder.binding,position)


        holder.itemView.setOnClickListener {
            selectItem(position)
        }

    }

    private fun setView(binding: SingleEnquiryLayoutBinding, position: Int) {

            if(data!![position].isSelected){
                binding.linearLayout.setBackgroundResource(R.drawable.btn_bg)
                binding.ENo.setTextColor(context.resources.getColor(R.color.black))
                binding.EDate.setTextColor(context.resources.getColor(R.color.black))
            }else{

                binding.linearLayout.setBackgroundResource(R.color.transpernt)
                binding.ENo.setTextColor(context.resources.getColor(R.color.white))
                binding.EDate.setTextColor(context.resources.getColor(R.color.white))
            }



    }

    private fun selectItem(position: Int) {

        for(i in data!!.indices){
            data!![i].isSelected = position==i
        }
        listener.onEnquiryClick(data!![position],position)
        notifyDataSetChanged()
    }

    interface EnquiryClick{
        fun onEnquiryClick(data: com.kspl.smitglobal.modelClasses.UserEnquiryResponse.Data,position: Int)
    }
}