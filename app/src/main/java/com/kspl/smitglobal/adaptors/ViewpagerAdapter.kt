package com.kspl.smitglobal.adaptors

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient.Companion.IMG_URL
import com.kspl.smitglobal.databinding.SingleViewpagerLayoutBinding
import com.squareup.picasso.Picasso

class ViewpagerAdapter(
    private var imagesArray: List<String>,
    var listener : Clicked
) : RecyclerView.Adapter<ViewpagerAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        var binding: SingleViewpagerLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.single_viewpager_layout,
            parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val current: String = IMG_URL + imagesArray[position]
        holder.binding.imageView7.isZoomable = true
        Picasso.get()
            .load(current)
            .error(R.drawable.logo)
            .placeholder(R.drawable.logo)
            .into(holder.binding.imageView7)



        holder.binding.imageView7.setOnClickListener {
            listener.onClicked()
        }

    }

    override fun getItemCount() = imagesArray.size

    class MyViewHolder(var binding: SingleViewpagerLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface Clicked {
        fun onClicked()
    }
}