package com.kspl.smitglobal.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.databinding.SingleCategoryLayoutBinding
import com.kspl.smitglobal.modelClasses.UsersResponse.Data


class UsersAdaptor(var data: List<Data>?,var itemlistener : ItemClick) : RecyclerView.Adapter<UsersAdaptor.MyViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdaptor.MyViewHolder {
        context = parent.context

        val binding: SingleCategoryLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_category_layout,
            parent, false
        )
        return UsersAdaptor.MyViewHolder(binding)

    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: UsersAdaptor.MyViewHolder, position: Int) {
        holder.binding.tvCategoryNumber.text = (position + 1).toString()
        holder.binding.tvCategoryName.text = data!!.get(position).name

        holder.binding.ivDelete.visibility= View.GONE

        holder.itemView.setOnClickListener {
            itemlistener.onItemClick(data!![position])
        }

    }


    class MyViewHolder(var binding: SingleCategoryLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface ItemClick{
        fun onItemClick (data: Data)
    }

}