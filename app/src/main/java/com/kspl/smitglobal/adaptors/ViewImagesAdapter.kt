package com.kspl.smitglobal.adaptors

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient.Companion.IMG_URL
import com.kspl.smitglobal.databinding.SingleImageLayoutBinding
import com.squareup.picasso.Picasso

class ViewImagesAdapter(
    var imagesArray: List<String>,
    var listener: ImageSelect
) : RecyclerView.Adapter<ViewImagesAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var binding : SingleImageLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.single_image_layout,
            parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val current: String = IMG_URL+imagesArray[position]
        Picasso.get()

            .load(current)
            .error(R.drawable.logo)
            .placeholder(R.drawable.logo)
            .into(holder.binding.productImage)

        holder.itemView.setOnClickListener {
            listener.onImageSelect(position)
        }
    }

    override fun getItemCount() = imagesArray.size

    class MyViewHolder(var binding: SingleImageLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    interface ImageSelect{
        fun onImageSelect(pos:Int)
    }
}