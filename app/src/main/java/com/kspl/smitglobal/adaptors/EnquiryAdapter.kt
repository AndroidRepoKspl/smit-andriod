package com.kspl.smitglobal.adaptors

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.devs.readmoreoption.ReadMoreOption
import com.kspl.smitglobal.R
import com.kspl.smitglobal.api.ApiClient
import com.kspl.smitglobal.databinding.SingleItemLayoutBinding
import com.kspl.smitglobal.modelClasses.getEnquiries.DataX
import com.kspl.smitglobal.modelClasses.products.Product
import com.kspl.smitglobal.utils.Constants
import com.preference.PowerPreference
import com.preference.provider.PreferenceProvider.context
import com.squareup.picasso.Picasso

class EnquiryAdapter(
    var list: List<DataX>?,
    var listener: EnquiryItemClick

) : RecyclerView.Adapter<EnquiryAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: SingleItemLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.single_item_layout, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.bindProduct(list!![position])
        holder.binding.tvSendEnquiry.setOnClickListener {
            listener.onEnquirySend(list!![position])
        }
    }

    override fun getItemCount(): Int = list?.size ?: 0

    class MyViewHolder(var binding: SingleItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bindProduct(product: DataX) {
            binding.tvSendEnquiry.text = "Delete Enquiry"
            binding.tvItemName.text = product?.name


            binding.tvItemPrice.text = "\u20B9${product!!.price} / (${product.unit} Unit)"

            if(product?.multiple_images!=null){
                if(product.multiple_images.isNotEmpty()){
                    updateWithUrl(ApiClient.IMG_URL +product.multiple_images[0])
                }else{
                    updateWithUrl(product.file_path)
                }
            }


            binding.ivSelected.visibility=View.INVISIBLE


            if (product?.description == null) {
                binding.tvDescription.visibility = View.GONE
            } else {
                binding.tvDescription.visibility = View.VISIBLE
                val readMoreOption = ReadMoreOption.Builder(context)
                    .textLength(1, ReadMoreOption.TYPE_LINE) // OR
                    //.textLength(3, ReadMoreOption.TYPE_CHARACTER)
                    .moreLabel("Read more")
                    .lessLabel("Read less")
                    .moreLabelColor(Color.WHITE)
                    .lessLabelColor(Color.WHITE)
                    .labelUnderLine(false)
                    .expandAnimation(true)
                    .build()
                readMoreOption.addReadMoreTo(
                    binding.tvDescription,
                    "${product.description}"
                )
            }

            if (PowerPreference.getDefaultFile().getString("role_id") == "2") {
                binding.layoutEditDelete.visibility = View.VISIBLE
            } else {
                binding.layoutEditDelete.visibility = View.GONE
            }

            if(product!!.product_grade ==null){
                binding.tvGarde.visibility = View.GONE
            }else
            {
                binding.tvGarde.visibility = View.VISIBLE
                binding.tvGarde.text = "Grade : ${product.product_grade}"
            }

            if(product.company_name ==null){
                binding.tvCompanyName.visibility = View.GONE
            }else
            {
                binding.tvCompanyName.visibility = View.VISIBLE
                binding.tvCompanyName.text = "Company Name : ${product.company_name}"
            }

            /*if(product.country_name ==null){
                binding.tvCountryName.visibility = View.GONE
            }else
            {
                binding.tvCountryName.visibility = View.VISIBLE
                binding.tvCountryName.text = "Country : ${product.country_name}"
            }*/


        }

        private fun updateWithUrl(url: String) {
            Picasso.get()
                .load(url)
                .error(R.drawable.square_logo)
                .placeholder(R.drawable.square_logo)
                .into(binding.ivItemImage)
        }

    }

    interface EnquiryItemClick {
        fun onEnquirySend(product: DataX)
    }
}
